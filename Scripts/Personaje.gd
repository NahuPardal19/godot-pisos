extends CharacterBody2D

class_name Player

#region Atributos
var stringInputPlayer : String

var gravity = 2200

var SPEED = 250.0
var FLOOR_SPEED = 100.0

var JumpCountMax : int = 2
var JumpCount : int = 0
var JumpForce = -500.0

@onready var audio_stream_player_2d: AudioStreamPlayer2D = $AudioStreamPlayer2D

@onready var Sprite : Sprite2D = $Sprite2D

@onready var coyote_jump_timer = $CoyoteJumpTimer


@onready var animation_player: AnimationPlayer = $AnimationPlayer

@onready var CanPlay:bool = true

var Lifes:int
var OnDie: bool

#endregion
enum PowerEnum{None,JumpPower,NoTouch,NoDamage,Health}
static var Power : PowerEnum
static var _power_sprite: Sprite2D

func _ready() -> void:
	GAME_MANAGER.InicioPartida.connect(_InicializarJugador)
	Power = PowerEnum.None
	_power_sprite = $PowerSprite
	stringInputPlayer = "Singleplayer_"

	
func _process(delta: float) -> void:
	_Gravity(delta)
	if CanPlay:
		if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
			if velocity.y > 0 and !is_on_floor():
				animation_player.play("Clown_Fall")
			elif velocity.y < 0 and !is_on_floor():
				animation_player.play("Clown_Jump")
		else:
			if velocity.y > 0 and !is_on_floor():
				animation_player.play("fall")
			elif velocity.y < 0 and !is_on_floor():
				animation_player.play("Jump")
	else: 
		if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
			animation_player.play("Clown_Idle")
		#else:
			#animation_player.play("idle")
	
	if OS.has_feature("editor"):
		if Input.is_action_just_pressed("NextLevel"):
			GAME_MANAGER.NextLevel()
		if Input.is_action_just_pressed("PreviousLevel"):
			GAME_MANAGER.PreviousLevel()
	
	if Input.is_action_just_pressed("Edit") and GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and GAME_MANAGER.LevelEdited and GAME_MANAGER.CanRestart and !UI_MANAGER.Menu.visible:
		GAME_MANAGER.LevelEdited = false
		GAME_MANAGER.Restart()
	
	if CanPlay and !OnDie:
		_Move()
		_Jump()
	#move_and_slide()
		#if GAME_MANAGER.FloorsToActivate.size() > 0 or GAME_MANAGER.ConvertFloors.size() > 0 :
			#if is_on_floor() or \
			#(GAME_MANAGER.FloorsToActivate.size()>0 and GAME_MANAGER.FloorsToActivate[GAME_MANAGER.FloorsToActivate.size()-1].is_in_group("Jump")):
				#_Undo()
	
	if Input.is_action_just_pressed("Singleplayer_Restart"):
			GAME_MANAGER.Restart()
	if Input.is_action_just_pressed("Singleplayer_Pause"):
			GAME_MANAGER.Pause()
#---------------------------------------------------------------------------------------------------------------------
	
func _Move():
	if CanPlay:
		var direction = Input.get_axis(stringInputPlayer + "Left", stringInputPlayer +"Right")
		
		if is_on_floor():
			if direction:
					velocity.x = direction * FLOOR_SPEED
					if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
						animation_player.play("Clown_Walk")
					else:
						animation_player.play("Walk")
			else:
				velocity.x = move_toward(velocity.x, 0, SPEED)
				if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
					animation_player.play("Clown_Idle")
				else:
					animation_player.play("idle")
		else:
			if direction:
				velocity.x = direction * SPEED
			else:
				velocity.x = move_toward(velocity.x, 0, SPEED)
		
			
		if direction > 0:
			Sprite.flip_h = false
		elif direction < 0:
			Sprite.flip_h = true
			
		var was_on_floor  = is_on_floor()
		
		move_and_slide()
		var just_left_ledge = was_on_floor and not is_on_floor()
		if just_left_ledge:
			coyote_jump_timer.start()
func _Jump():
	if is_on_floor() and JumpCount != 0 :
		JumpCount = 0
	
	if Input.is_action_just_pressed(stringInputPlayer + "Jump"):
			
		if !is_on_floor() and JumpCount == 0 and coyote_jump_timer.time_left <= 0.0:
			JumpCount += 2
		if JumpCount < JumpCountMax or coyote_jump_timer.time_left > 0.0:
			velocity.y = 0  
			if Power == PowerEnum.JumpPower:
				JumpForce = -700
			else:
				JumpForce = -500
			audio_stream_player_2d.stream = AUDIO_MANAGER.Player_Jump
			audio_stream_player_2d.play()
			velocity.y = JumpForce 
			JumpCount += 1 			
func _Gravity(delta):
		#if not is_on_floor():
	velocity.y += gravity * delta
	if !CanPlay:
		velocity.x = 0
		move_and_slide()
func _Undo():
	if Input.is_action_just_pressed("Undo"):
		if GAME_MANAGER.UndoAmount > 0:
			GAME_MANAGER.Undo()
func _InicializarJugador():
	GAME_MANAGER.PlayerList.append(get_node("."))
	
func TestLife():
	if Lifes <= 0 && !OnDie:
		OnDie = true
		GAME_MANAGER.Loose()
static func ActivatePower(PowerToActivate : PowerEnum):
	Power = PowerToActivate
	
	if PowerToActivate == PowerEnum.JumpPower:
		_power_sprite.texture = load("res://Sprites/Powers icons/Jump Force.png")
	elif PowerToActivate == PowerEnum.NoTouch:
		_power_sprite.texture = load("res://Sprites/Powers icons/Shield Block.png")
	elif PowerToActivate == PowerEnum.None:
		_power_sprite.texture = null
	
	
static func DesactivatePower():
	if Power != PowerEnum.None:
		Power = PowerEnum.None
		_power_sprite.texture = null
