extends TextureRect

class_name Card

@onready var Actived:bool = true

@onready var animation_player: AnimationPlayer = $AnimationPlayer
@export_dir var floorPath
@export var ColorFloor : Floor.ColorEnum

@export var greenCard:Texture2D
@export var yellowCard:Texture2D
@export var whiteCard : Texture2D

@export var Hidden : bool 
@onready var Whiteback = preload("res://Sprites/Cards/Back White.png")
@onready var Greenback = preload("res://Sprites/Cards/Back Green.png")
@onready var Yellowback = preload("res://Sprites/Cards/Back Yellow.png")

@onready var fondo = $"."
@onready var angle = 0
var initial_y_position

const SHAKE_FREQUENCY = 20
const SHAKE_AMPLITUDE = 20
var OnShake: bool = false
var time_passed: float = 0.0

# Amplitud de la onda
#var amplitude: float = 10.0
## Frecuencia de la onda
#var frequency: float = 1.0
## Velocidad de la onda
#var speed: float = 8.0
## Variable para el tiempo
#var time_passed: float = 0.0
#var OnShake: bool = false

func _ready() -> void:
	
	initial_y_position = global_position.y
	
	if ColorFloor == Floor.ColorEnum.Green:
		$".".texture = greenCard
	if ColorFloor == Floor.ColorEnum.Yellow:
		$".".texture = yellowCard
	if ColorFloor == Floor.ColorEnum.White:
		$".".texture = whiteCard
		
		
	if Hidden:
		Hide()
		

func _process(delta: float) -> void:
	if OnShake:
		time_passed += delta
		var movement = cos(time_passed *  SHAKE_FREQUENCY) * SHAKE_AMPLITUDE
		rotation_degrees += movement*delta*4
	else:
		rotation_degrees = 0
		#position.x += movement * delta
		#position.y += movement * delta
	   # Actualizar el tiempo
		#time_passed += delta * speed
		## Calcular el desplazamiento en Y usando una función seno para crear la onda
		#var wave_offset = sin(time_passed * frequency) * amplitude
		## Aplicar el desplazamiento al nodo
		#position.y = wave_offset

func Hide():
	if ColorFloor == Floor.ColorEnum.Green:
		$".".texture = Greenback
	elif ColorFloor == Floor.ColorEnum.Yellow:
		$".".texture = Yellowback
	else:
		$".".texture = Whiteback
		
func ShowCard():
	if ColorFloor == Floor.ColorEnum.Green:
		$".".texture = greenCard
	elif ColorFloor == Floor.ColorEnum.Yellow:
		$".".texture = yellowCard
	else: $".".texture = whiteCard

func Destroy():
	#animation_player.active = true
	#get_parent().remove_child(get_node("."))
	#animation_player.stop()
	animation_player.play("Disolve")
	Actived=false
	await animation_player.animation_finished
	hide()

func Active():
	Actived = true
	material.set("shader_parameter/progress",-1)
	show()
