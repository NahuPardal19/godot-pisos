extends Area2D

@onready var audio_stream_player_2d = $AudioStreamPlayer2D

func _ready():
	audio_stream_player_2d.stream = AUDIO_MANAGER.Player_DeadFall

func _on_body_entered(body):
	if body.is_in_group("Player"):
		audio_stream_player_2d.play()
		await audio_stream_player_2d.finished
		GAME_MANAGER.Restart()
