extends Floor

#FLOOR 

var InicioNivel : Node2D
@export var Indestructible : bool
var touch_win : bool = false
func _ready():
	#SETEO SPRITES Y AUDIO
	Sprite = $"Floor Sprite"
	GAME_MANAGER.PasoRonda.connect(Actualizar)
	AudioPlayer.stream = AUDIO_MANAGER.Player_RoundPassed
	super()
	if !Indestructible and GAME_MANAGER.Stage != GAME_MANAGER.StageEnum.LevelEditorStage:
		modulate = Color(0.29, 0.29, 0.29)

func Activate():
	if !Indestructible:
		if connector != null:
			connector.EliminateConnector(get_node("."))
		FloorAnimator.play("Destroy")
		await FloorAnimator.animation_finished
		_Destroy()

func Touch(body):
	if !touch_win:
		if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
			AudioPlayer.stream = AUDIO_MANAGER.Player_RoundPassed
		super(body)
		if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
			body.animation_player.play("idle")
			touch_win = true
			GAME_MANAGER.StopPlayer(false,GAME_MANAGER.PlayerList[0])
			GAME_MANAGER.CanPause = false
			await AudioPlayer.finished
			GAME_MANAGER.PasarRonda(body)
			Touched = false

func Actualizar():
	touch_win = false

func TouchWithConnector(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and !Touched:
		Sprite.texture = TouchedSprite
		if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			GAME_MANAGER.AgregarPiso(get_node("."))
				
		Touched = false
		GAME_MANAGER.LastFloorTouched = get_node(".")
				
		body.DesactivatePower()
		SHEET_MANAGER.metadata_BlocksTouched += 1
		
		AudioPlayer.stream = AUDIO_MANAGER.Player_RoundPassed
		AudioPlayer.play()
		
		body.animation_player.play("idle")
		GAME_MANAGER.StopPlayer(false,GAME_MANAGER.PlayerList[0])
		GAME_MANAGER.CanPause = false
		await AudioPlayer.finished
		GAME_MANAGER.PasarRonda(body)
	
func PutInPreparation():
	pass
