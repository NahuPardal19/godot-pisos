extends Floor

#FLOOR

@export var yellow:Color
@export var green:Color

@export var Texture_0_Touched : Texture2D
@export var Texture_1_Touched : Texture2D
@export var Texture_2_Touched : Texture2D
@export var Texture_3_Touched : Texture2D
@export var Texture_4_Touched : Texture2D
@export var Texture_5_Touched : Texture2D
@export var Texture_0 : Texture2D
@export var Texture_1 : Texture2D
@export var Texture_2 : Texture2D
@export var Texture_3 : Texture2D
@export var Texture_4 : Texture2D
@export var Texture_5 : Texture2D

@export var NumeroInicial : int
var numeroActual : int

func _ready():
	Sprite = $Sprite2D
	numeroActual = NumeroInicial
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
	super()

func _restarNumero():
	numeroActual -= 1
	_PonerNumero()
	
func _PonerNumero():
	if Touched:
		if numeroActual == 0:
			Sprite.texture = Texture_0_Touched
		elif numeroActual == 1:
			Sprite.texture = Texture_1_Touched
		elif numeroActual == 2:
			Sprite.texture = Texture_2_Touched
		elif numeroActual == 3:
			Sprite.texture = Texture_3_Touched
		elif numeroActual == 4:
			Sprite.texture = Texture_4_Touched
		elif numeroActual == 5:
			Sprite.texture = Texture_5_Touched	
	else:
		if numeroActual == 0:
			Sprite.texture = Texture_0
		elif numeroActual == 1:
			Sprite.texture = Texture_1
		elif numeroActual == 2:
			Sprite.texture = Texture_2
		elif numeroActual == 3:
			Sprite.texture = Texture_3
		elif numeroActual == 4:
			Sprite.texture = Texture_4
		elif numeroActual == 5:
			Sprite.texture = Texture_5

func Activate():
	Touched = false
	_PonerNumero()
	if numeroActual <= 0:
		if connector != null:
			connector.EliminateConnector(get_node("."))
		FloorAnimator.play("Destroy")
		await FloorAnimator.animation_finished
		_Destroy()
func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		if !Touched:
			super(body)
			if connector == null:
				_restarNumero()
func TouchWithConnector(body):
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
	if !Touched:
		super(body)
		_restarNumero()


func Restart():
	super()
	numeroActual = NumeroInicial
	_PonerNumero()

