extends Floor

@export var JumpForce : int
@onready var CanJump:bool = true

func _ready():
	AudioPlayer.stream = AUDIO_MANAGER.JumpBoost
	Sprite = $Sprite2D
	super()

func Activate():
	if connector != null:
		connector.EliminateConnector(get_node("."))
	FloorAnimator.play("Destroy")
	await FloorAnimator.animation_finished
	queue_free()
func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and !Touched:
		
		if connector != null:
			connector.TouchFloors(body)
			return
		
		Player.ActivatePower(Player.PowerEnum.JumpPower)
		AudioPlayer.play()
		SHEET_MANAGER.metadata_BlocksTouched += 1
		Touched = true
		Sprite.texture = TouchedSprite
		GAME_MANAGER.LastFloorTouched = get_node(".")
		
		if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			GAME_MANAGER.AgregarPiso(get_node("."))
func TouchWithConnector(_body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and !Touched:
		Sprite.texture = TouchedSprite
		
		if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			GAME_MANAGER.AgregarPiso(get_node("."))
			
		AudioPlayer.play()
		Touched = true
		GAME_MANAGER.LastFloorTouched = get_node(".")
		
		Player.ActivatePower(Player.PowerEnum.JumpPower)
		SHEET_MANAGER.metadata_BlocksTouched += 1

func Restart():
	Touched = false
	Sprite.texture = InitialWhiteSprite
	SHEET_MANAGER.metadata_BlocksTouched -= 1
