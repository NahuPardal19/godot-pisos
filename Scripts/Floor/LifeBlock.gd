extends Floor

@export var LifesToQuit : int

func _ready():
	AudioPlayer.stream = AUDIO_MANAGER.Player_LifeQuit
	Sprite = %Sprite2D
	super()

				
func Activate():
	if connector != null:
		connector.EliminateConnector(get_node("."))
	FloorAnimator.play("Destroy")
	await FloorAnimator.animation_finished
	_Destroy()

func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		AudioPlayer.stream = AUDIO_MANAGER.Player_LifeQuit
		if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and !Touched and connector == null:
			AudioPlayer.stream = AUDIO_MANAGER.Player_LifeQuit
			body.Lifes -= LifesToQuit
			UI_MANAGER.QuitLife()
			UI_MANAGER.PutLife(body.Lifes)
			body.TestLife()
		super(body)
	

func TouchWithConnector(body):
	if !Touched:
		body.Lifes -= LifesToQuit
		UI_MANAGER.QuitLife()
		UI_MANAGER.PutLife(body.Lifes)
		body.TestLife()
	super(body)

