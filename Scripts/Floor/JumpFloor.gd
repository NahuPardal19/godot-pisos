extends Floor

@export var JumpForce : int
@onready var CanJump:bool = true

func _ready():
	#AudioPlayer.stream = AUDIO_MANAGER.JumpBoost
	Sprite = $Sprite2D
	super()

func Activate():
	if connector != null:
		connector.EliminateConnector(get_node("."))
	FloorAnimator.play("Destroy")
	await FloorAnimator.animation_finished
	_Destroy()
func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		pass
		#AudioPlayer.stream = AUDIO_MANAGER.JumpdBoost
	super(body)
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		body.velocity.y = JumpForce 
