extends StaticBody2D

class_name Floor

var Touched : bool

@onready var AudioPlayer : AudioStreamPlayer2D = $AudioStreamPlayer2D
@onready var FloorAnimator: AnimationPlayer = $AnimationPlayer

var Sprite : Sprite2D

var connector : Connector

enum ColorEnum{White,Green,Yellow}
@export var color : ColorEnum

@export var InitialWhiteSprite : Texture2D
@export var InitialYellowSprite : Texture2D
@export var InitialGreenSprite : Texture2D

@export var TouchedSprite : Texture2D 

func _ready() -> void:
	
	GAME_MANAGER.FinPartida.connect(Delete)
	GAME_MANAGER.InicioPreparationStage.connect(PutInPreparation)
	GAME_MANAGER.InicioGameStage.connect(PutInGame)
	GAME_MANAGER.CambioCarta.connect(PutInPreparation)
	_init_sprite()
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		PutInPreparation()

func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		if Player.Power != Player.PowerEnum.NoTouch:
			Touch(body)
		else:
			Player.DesactivatePower()

func Activate():
	pass

func Touch(body):
	if	 GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and !Touched:
		
		if connector != null:
			connector.TouchFloors(body)
			return
		
		body.DesactivatePower()
		AudioPlayer.play()
		SHEET_MANAGER.metadata_BlocksTouched += 1
		Touched = true
		Sprite.texture = TouchedSprite
		GAME_MANAGER.LastFloorTouched = get_node(".")
		
		if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			GAME_MANAGER.AgregarPiso(get_node("."))

func TouchWithConnector(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and !Touched:
		Sprite.texture = TouchedSprite
		
		if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			GAME_MANAGER.AgregarPiso(get_node("."))
			
		AudioPlayer.play()
		Touched = true
		GAME_MANAGER.LastFloorTouched = get_node(".")
		
		body.DesactivatePower()
		SHEET_MANAGER.metadata_BlocksTouched += 1

func Restart():
	FloorAnimator.stop()
	$Area2D.set_collision_layer_value(1,true)
	$".".set_collision_layer_value(1,true)
	$Area2D.set_collision_mask_value(1,true)
	$".".set_collision_mask_value(1,true)
	Touched = false
	SHEET_MANAGER.metadata_BlocksTouched -= 1
	_init_sprite()
	scale = Vector2.ONE
	show()
	if connector != null:
		connector.Restart()

func Respawn():
	$Area2D.set_collision_mask_value(1,true)
	$".".set_collision_mask_value(1,true)
	$Area2D.set_collision_layer_value(1,true)
	$".".set_collision_layer_value(1,true)
	show()
func Delete():
	queue_free()
	
func PutInPreparation():
	_init_sprite()
	modulate = Color(0.29, 0.29, 0.29)
	if connector != null:
		connector.modulate = Color(0.29, 0.29, 0.29)
	_init_sprite()

func PutInGame():
	_init_sprite()
	modulate = Color(1, 1, 1)
	if connector != null:
		connector.modulate = Color(1,1,1)
	if !GAME_MANAGER.LevelEdited:
		GAME_MANAGER.BlocksLevelEdited.append(get_node(".").duplicate())

func Undo_Prep():
	var floorToConvert
	if color == ColorEnum.White:
		floorToConvert = load("res://Scenes/Floors/Normal floor/WhiteFloor.tscn")
	elif color == ColorEnum.Green:
		floorToConvert = load("res://Scenes/Floors/Normal floor/GreenFloor.tscn")
	elif color == ColorEnum.Yellow:
		floorToConvert = load("res://Scenes/Floors/Normal floor/YellowFloor.tscn")
	#floorInstance.audio_stream_player_2d.stream = AUDIO_MANAGER.PutCard
	#floorInstance.audio_stream_player_2d.play()
	var floorInstance = floorToConvert.instantiate()
	get_tree().get_root().add_child(floorInstance)
	floorInstance.global_position = global_position
	floorInstance.modulate = Color(0.29, 0.29, 0.29)
	queue_free()

func _init_sprite():
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation or GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.LevelEditorStage or GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Menu:
		if color == Floor.ColorEnum.White:
			Sprite.texture = InitialWhiteSprite
		elif color == Floor.ColorEnum.Green:
			Sprite.texture = InitialGreenSprite
		elif color == Floor.ColorEnum.Yellow:
			Sprite.texture = InitialYellowSprite
	else:
		Sprite.texture = InitialWhiteSprite

func _Destroy():
	hide()
	$Area2D.set_collision_layer_value(1,false)
	$".".set_collision_layer_value(1,false)
	$Area2D.set_collision_mask_value(1,false)
	$".".set_collision_mask_value(1,false)
