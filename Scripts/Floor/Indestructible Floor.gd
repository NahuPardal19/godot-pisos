extends Floor

@onready var ray_cast_2d: RayCast2D = $RayCast2D

func _ready():
	Sprite = %Sprite2D
	super()
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound

func Activate():
	if connector != null:
		connector.EliminateConnector(get_node("."))
	FloorAnimator.play("Destroy")
	await FloorAnimator.animation_finished
	queue_free()

func Convert(card:Card):
	if color == card.ColorFloor:
		
		Touched = true
		GAME_MANAGER.QuitCard(card)
		
		var floorToConvert = load(card.floorPath)
		var floorInstance = floorToConvert.instantiate()
		floorInstance.color = card.ColorFloor
		get_tree().get_root().call_deferred("add_child", floorInstance)
		while not floorInstance.is_inside_tree():
			await get_tree().process_frame
		
		floorInstance.AudioPlayer.stream = AUDIO_MANAGER.PutCard
		floorInstance.AudioPlayer.play()
		floorInstance.global_position = global_position
		floorInstance.modulate = Color(0.29, 0.29, 0.29)
	
		GAME_MANAGER.ConvertFloors.append(floorInstance)
		
		if connector != null:
			connector.RemoveFloor($".")
			connector.AddFloor(floorInstance)
		
		queue_free()
		
func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and !Touched:
		
		if connector != null:
			connector.TouchFloors(body)
			return
			
		body.DesactivatePower()
		AudioPlayer.play()
		SHEET_MANAGER.metadata_BlocksTouched += 1
			
	#if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		#if GAME_MANAGER.GetCardsOnDeck().size()>0 and !Touched:
			#Convert(GAME_MANAGER.GetActiveCard())
func PutInPreparation():
	pass
	#if GAME_MANAGER.GetCardsOnDeck().size() > 0:
		#if GAME_MANAGER.GetActiveCard().ColorFloor != color:
			#modulate = Color(0.29, 0.29, 0.29)
			#if connector != null:
				#connector.modulate = Color(0.29, 0.29, 0.29)
		#else:
			#modulate = Color(1,1,1)
			#if connector != null:
				#connector.modulate = Color(1,1,1)
	#else:
		#modulate = Color(0.29, 0.29, 0.29)
		#if connector != null:
			#connector.modulate = Color(0.29,0.29,0.29)
	#if ray_cast_2d.is_colliding():
		#modulate = Color(0.29, 0.29, 0.29)
		#if connector != null:
				#connector.modulate = Color(0.29, 0.29, 0.29)

