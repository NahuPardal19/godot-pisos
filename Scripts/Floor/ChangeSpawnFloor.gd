extends Floor

@export var Active : bool
@onready var spawn: Node2D = $NewSpawn

func _ready() -> void:
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
	Sprite = %Sprite2D
	GAME_MANAGER.InicioGameStage.connect(PutInGame)
	if Active:
		GAME_MANAGER.Spawner = $"."
		GAME_MANAGER.InitialSpawner = $"."
	if GAME_MANAGER.StageEnum.LevelEditorStage != GAME_MANAGER.Stage and scene_file_path == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn":
		$Sprite2D2.hide()

func _process(delta: float) -> void:
	if Active and GAME_MANAGER.StageEnum.LevelEditorStage == GAME_MANAGER.Stage:
		if $RayCast2D.is_colliding():
			LevelEditor.DestroyBlock($RayCast2D.get_collider())
	if GAME_MANAGER.StageEnum.LevelEditorStage != GAME_MANAGER.Stage and scene_file_path == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn":
		$Sprite2D2.hide()

func Activate():
	pass

func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		if !Active and !Touched:
			super(body)
			GAME_MANAGER.PosicionInicial = $NewSpawn
			GAME_MANAGER.Spawner.Sprite.texture = InitialWhiteSprite
			GAME_MANAGER.Spawner.Active = false
			GAME_MANAGER.Spawner.Touched = false
			GAME_MANAGER.Spawner = $"."
			Active = true
			_init_sprite()
			
func TouchWithConnector(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		if !Active:
			super(body)
			GAME_MANAGER.PosicionInicial = $NewSpawn
			GAME_MANAGER.Spawner.Sprite.texture = InitialWhiteSprite
			GAME_MANAGER.Spawner.Active = false
			GAME_MANAGER.Spawner.Touched = false
			GAME_MANAGER.Spawner = $"."
			Active = true

func _init_sprite():
	if Active:
		Sprite.texture = TouchedSprite
	else:
		Sprite.texture = InitialWhiteSprite

func Restart():
	super()
	if GAME_MANAGER.InitialSpawner != $".":
		Active = false
	_init_sprite()
