extends Floor

class_name ChangeFloor

@onready var audio_stream_player_2d = $AudioStreamPlayer2D
@onready var animation_player: AnimationPlayer = $AnimationPlayer

@onready var arrows: Sprite2D = %Arrows


@export var arrowNormal : Texture2D
@export var arrowActivated : Texture2D

@export var activated:bool
var ToActive : bool

static var ChangeFloors:Array[ChangeFloor]

func _ready():
	audio_stream_player_2d.stream = AUDIO_MANAGER.FloorSound
	Sprite = %Sprite2D
	
	super()
	
	ChangeFloors.append(get_node("."))
	GAME_MANAGER.PasoRonda.connect(Active)


func _on_area_2d_body_entered(body):
	if !Touched:
		if body.is_in_group("Player"):
			Touch(body)
				
func Activate():
	if connector != null:
		connector.get_child(0).modulate.a = 100
	Touched = false
	activated = false
	animation_player.play("Change off")
	
	$".".set_collision_layer_value(1,false)
	$".".set_collision_mask_value(1,false)
	
	$Area2D.set_collision_layer_value(1,false)
	$Area2D.set_collision_mask_value(1,false)
	
	await animation_player.animation_finished
	arrows.texture = arrowNormal
	
	if color == Floor.ColorEnum.White:
		Sprite.texture = InitialWhiteSprite
	elif color == Floor.ColorEnum.Green:
		Sprite.texture = InitialGreenSprite
	elif color == Floor.ColorEnum.Yellow:
		Sprite.texture = InitialYellowSprite
func Active():
	if ToActive:
		animation_player.play("Change On")
		$".".set_collision_layer_value(1,true)
		$".".set_collision_mask_value(1,true)
	
		$Area2D.set_collision_layer_value(1,true)
		$Area2D.set_collision_mask_value(1,true)
		arrows.texture = arrowNormal
		activated = true
		ToActive = false

func Touch(body):	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game && !Touched:
		if connector != null:
			connector.TouchFloors(body)
			return
		
		Sprite.texture = TouchedSprite
		
		for floorP in ChangeFloors:
			if !floorP.activated:
				floorP.arrows.texture = floorP.arrowActivated
				floorP.ToActive = true
			else:
				floorP.TouchWithChangeFloor(body)
		
		if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			GAME_MANAGER.AgregarPiso(get_node("."))
			
		audio_stream_player_2d.play()
		Touched = true
		SHEET_MANAGER.metadata_BlocksTouched += 1
func TouchWithChangeFloor(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game && !Touched:
		if connector != null:
			connector.TouchFloors(body)
			return
		
		Sprite.texture = TouchedSprite
		
		#if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			#GAME_MANAGER.AgregarPiso(get_node("."))
			
		audio_stream_player_2d.play()
		Touched = true
		SHEET_MANAGER.metadata_BlocksTouched += 1
		
func TouchWithConnector(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game && !Touched:
		Sprite.texture = TouchedSprite
		Touched = true
		
		for floorP in ChangeFloors:
			if !floorP.activated:
				floorP.arrows.texture = floorP.arrowActivated
				floorP.ToActive = true
			else:
				floorP.TouchWithChangeFloor(body)
		
		if !GAME_MANAGER.FloorsToActivate.has(get_node(".")):
			GAME_MANAGER.AgregarPiso(get_node("."))
		SHEET_MANAGER.metadata_BlocksTouched += 1
		audio_stream_player_2d.play()
		

func Restart():
	if Touched:
		Touched = false
		for floorP in ChangeFloors:
			if floorP.color == Floor.ColorEnum.White:
				floorP.Sprite.texture = floorP.InitialWhiteSprite
			elif floorP.color == Floor.ColorEnum.Green:
				floorP.Sprite.texture = floorP.InitialGreenSprite
			elif floorP.color == Floor.ColorEnum.Yellow:
				floorP.Sprite.texture = floorP.InitialYellowSprite
			floorP.Touched = false
			SHEET_MANAGER.metadata_BlocksTouched -= 1
	else:
		if color == Floor.ColorEnum.White:
				Sprite.texture = InitialWhiteSprite
		elif color == Floor.ColorEnum.Green:
				Sprite.texture = InitialGreenSprite
		elif color == Floor.ColorEnum.Yellow:
				Sprite.texture = InitialYellowSprite
		SHEET_MANAGER.metadata_BlocksTouched -= 1
