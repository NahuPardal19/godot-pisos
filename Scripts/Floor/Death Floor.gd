extends  Floor

func _ready():
	Sprite = %Sprite2D
	super()
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound

	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		PutInPreparation()

func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		body.animation_player.stop()
		body.OnDie = true
		GAME_MANAGER.StopPlayer(false , GAME_MANAGER.PlayerList[0])
		AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
		super(body)
		body.animation_player.play("idle")
		GAME_MANAGER.Loose()
		
func TouchWithConnector(body):
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
	super(body)
	GAME_MANAGER.Loose()
