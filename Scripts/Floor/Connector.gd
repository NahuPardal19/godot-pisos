extends Area2D

class_name Connector

enum TypeConnectorEnum{Blue,Yellow,Red,Green}
@export var ConnectorType : TypeConnectorEnum
@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D
var ReadyForConnect : bool

@onready var sprite: Sprite2D = $Sprite2D

static var BlueFloors : Array[Floor] = []
static var YellowFloors : Array[Floor] = []
static var RedFloors : Array[Floor] = []
static var GreenFloors : Array[Floor] = []

@onready var animation_player: AnimationPlayer = $AnimationPlayer


func _ready() -> void:
	BlueFloors.clear()
	YellowFloors.clear()
	RedFloors.clear()
	GreenFloors.clear()
	
	ReadyForConnect = true
	
	if ConnectorType == TypeConnectorEnum.Blue:
		sprite.texture = load("res://Sprites/Floors sprites/Connectors sprites/Connector Blue.png") 
	elif ConnectorType == TypeConnectorEnum.Green:
		sprite.texture = load("res://Sprites/Floors sprites/Connectors sprites/Connector Green.png") 
	elif ConnectorType == TypeConnectorEnum.Red:
		sprite.texture = load("res://Sprites/Floors sprites/Connectors sprites/Connector Red.png") 
	elif ConnectorType == TypeConnectorEnum.Yellow:
		sprite.texture = load("res://Sprites/Floors sprites/Connectors sprites/Connector Yellow.png") 

#func _process(delta: float) -> void:
	#print(GreenFloors.size())


func _on_body_entered(body: Node2D) -> void:
	print("agrego " + body.get_name())
	if ConnectorType == TypeConnectorEnum.Blue:
		BlueFloors.append(body)
		body.connector = get_node(".")
		#collision_shape_2d.queue_free()
		ReadyForConnect = false
	elif ConnectorType == TypeConnectorEnum.Yellow:
		YellowFloors.append(body)
		body.connector = get_node(".")
		#collision_shape_2d.queue_free()
		ReadyForConnect = false
	elif ConnectorType == TypeConnectorEnum.Red:
		RedFloors.append(body)
		body.connector = get_node(".")
		#collision_shape_2d.queue_free()
		ReadyForConnect = false
	elif ConnectorType == TypeConnectorEnum.Green:
		GreenFloors.append(body)
		body.connector = get_node(".")
		#collision_shape_2d.queue_free()
		ReadyForConnect = false
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		body.PutInPreparation()
		
func TouchFloors(body):
	print("green:" + str(GreenFloors.size()))
	if ConnectorType == TypeConnectorEnum.Blue:
		for floorp in BlueFloors:
			floorp.TouchWithConnector(body)
	elif ConnectorType == TypeConnectorEnum.Yellow:
		for floorp in YellowFloors:
			floorp.TouchWithConnector(body)
	elif ConnectorType == TypeConnectorEnum.Red:
		for floorp in RedFloors:
			floorp.TouchWithConnector(body)
	elif ConnectorType == TypeConnectorEnum.Green:
		for floorp in GreenFloors:
			floorp.TouchWithConnector(body)
func ActivateConnectors():
	if ConnectorType == TypeConnectorEnum.Blue:
		for floorP in BlueFloors:
			floorP.Activate()
	elif ConnectorType == TypeConnectorEnum.Green:
		for floorP in GreenFloors:
			floorP.Activate()
	elif ConnectorType == TypeConnectorEnum.Red:
		for floorP in RedFloors:
			floorP.Activate()
	elif ConnectorType == TypeConnectorEnum.Yellow:
		for floorP in YellowFloors:
			floorP.Activate()
func EliminateConnector(floorP:Floor):
	if ConnectorType == TypeConnectorEnum.Blue:
		BlueFloors.erase(floorP)
		animation_player.play("Destroy")
		await animation_player.animation_finished
		_Destroy()
	elif ConnectorType == TypeConnectorEnum.Green:
		GreenFloors.erase(floorP)
		animation_player.play("Destroy")
		await animation_player.animation_finished
		_Destroy()
	elif ConnectorType == TypeConnectorEnum.Red:
		RedFloors.erase(floorP)
		animation_player.play("Destroy")
		await animation_player.animation_finished
		_Destroy()
	elif ConnectorType == TypeConnectorEnum.Yellow:
		YellowFloors.erase(floorP)
		animation_player.play("Destroy")
		await animation_player.animation_finished
		_Destroy()
func RestartConnectors():
	if ConnectorType == TypeConnectorEnum.Blue:
		for floorP in BlueFloors:
			floorP.Restart()
			GAME_MANAGER.FloorsToActivate.erase(floorP)
	elif ConnectorType == TypeConnectorEnum.Green:
		for floorp in GreenFloors:
			floorp.Restart()
			GAME_MANAGER.FloorsToActivate.erase(floorp)
	elif ConnectorType == TypeConnectorEnum.Red:
		for floorp in RedFloors:
			floorp.Restart()
			GAME_MANAGER.FloorsToActivate.erase(floorp)
	elif ConnectorType == TypeConnectorEnum.Yellow:
		for floorp in YellowFloors:
			floorp.Restart()
			GAME_MANAGER.FloorsToActivate.erase(floorp)

func AddFloor(floorP:Floor):
	print("agrego " + floorP.get_name())
	if ConnectorType == TypeConnectorEnum.Blue:
		BlueFloors.append(floorP)
		floorP.connector = get_node(".")
	elif ConnectorType == TypeConnectorEnum.Yellow:
		YellowFloors.append(floorP)
		floorP.connector = get_node(".")
	elif ConnectorType == TypeConnectorEnum.Red:
		RedFloors.append(floorP)
		floorP.connector = get_node(".")
	elif ConnectorType == TypeConnectorEnum.Green:
		GreenFloors.append(floorP)
		floorP.connector = get_node(".")	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		floorP.PutInPreparation()
func RemoveFloor(floorP:Floor):
	print("removio " + floorP.get_name())
	if ConnectorType == TypeConnectorEnum.Blue:
		BlueFloors.erase(floorP)
	elif ConnectorType == TypeConnectorEnum.Green:
		GreenFloors.erase(floorP)
	elif ConnectorType == TypeConnectorEnum.Red:
		RedFloors.erase(floorP)
	elif ConnectorType == TypeConnectorEnum.Yellow:
		YellowFloors.erase(floorP)

func Restart():
	show()
	$".".set_collision_layer_value(1,true)
	$".".set_collision_mask_value(1,true)
	scale = Vector2.ONE
func _Destroy():
	hide()
	$".".set_collision_layer_value(1,false)
	$".".set_collision_mask_value(1,false)
