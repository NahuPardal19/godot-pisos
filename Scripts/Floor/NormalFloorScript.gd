extends Floor

@onready var ray_cast_2d: RayCast2D = $RayCast2D
@onready var outline: Sprite2D = %Outline

func _ready():
	Sprite = %Sprite2D
	super()
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound

		
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		PutInPreparation()
func _process(_delta: float) -> void:
	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		if ray_cast_2d.is_colliding():
			if ray_cast_2d.get_collider() != null and ray_cast_2d.get_collider().get_name() != "Player":
				modulate = Color(0.29, 0.29, 0.29)
				outline.hide()
		else:
			PutInPreparation()
	print(color)
	print("------")

func Activate():
	if connector != null:
		connector.EliminateConnector(get_node("."))
	FloorAnimator.play("Destroy")
	await FloorAnimator.animation_finished
	_Destroy()

func Convert(card:Card):
	if color == card.ColorFloor:
		outline.hide()
		modulate = Color(1,1,1)
		Touched = true
		GAME_MANAGER.QuitCard(card)
		if GAME_MANAGER.GetCardsOnDeck().size() <= 0:
			GAME_MANAGER.PlayerList[0].animation_player.play("Clown_Idle")
			GAME_MANAGER.StopPlayer(false , GAME_MANAGER.PlayerList[0])
		var floorToConvert = load(card.floorPath)
		var floorInstance = floorToConvert.instantiate()
		floorInstance.color = card.ColorFloor
		get_parent().call_deferred("add_child", floorInstance)
		
		while not floorInstance.is_inside_tree():
			await get_tree().process_frame
		
		
		floorInstance.AudioPlayer.stream = AUDIO_MANAGER.Card_Disolve
		floorInstance.AudioPlayer.play()
		floorInstance.global_position = global_position
		floorInstance.modulate = Color(0.29, 0.29, 0.29)
		outline.hide()
	
		GAME_MANAGER.ConvertFloors.append(floorInstance)
		
		if connector != null:
			connector.RemoveFloor($".")
			connector.AddFloor(floorInstance)
		
		FloorAnimator.play("Convert")
		await FloorAnimator.animation_finished
		

		queue_free()
		
func Touch(body):
	outline.hide()
	super(body)
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		if GAME_MANAGER.GetCardsOnDeck().size()>0 and !Touched:
			outline.hide()
			Convert(GAME_MANAGER.GetActiveCard())

func PutInPreparation():
	if GAME_MANAGER.GetCardsOnDeck().size() > 0:
		if GAME_MANAGER.GetActiveCard().ColorFloor != color:
			modulate = Color(0.29, 0.29, 0.29)
			outline.hide()
			if connector != null:
				connector.modulate = Color(0.29, 0.29, 0.29)
		else:
			modulate = Color(1,1,1)
			if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation: outline.show()
			if connector != null:
				connector.modulate = Color(1,1,1)
	else:
		modulate = Color(0.29, 0.29, 0.29)
		outline.hide()
		if connector != null:
			connector.modulate = Color(0.29,0.29,0.29)
	if ray_cast_2d.is_colliding():
		modulate = Color(0.29, 0.29, 0.29)
		outline.hide()
		if connector != null:
				connector.modulate = Color(0.29, 0.29, 0.29)
	_init_sprite()
