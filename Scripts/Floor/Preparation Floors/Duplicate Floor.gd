extends PreparationFloor

func Touch(body):
	super(body)
	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		
		var cardDuplicate = load(GAME_MANAGER.GetActiveCard().scene_file_path)
		var cardDuplicateInstance = cardDuplicate.instantiate()
		cardDuplicateInstance.ColorFloor = GAME_MANAGER.GetActiveCard().ColorFloor
		cardDuplicateInstance.position = GAME_MANAGER.GetActiveCard().position
		GAME_MANAGER.GetActiveCard().get_parent().add_child(cardDuplicateInstance)
		
		GAME_MANAGER.GetActiveCard().OnShake = false
		cardDuplicateInstance.OnShake = false
		print("pos"+str(GAME_MANAGER.GetActiveCard().global_position))
		GAME_MANAGER.Deck.insert(1,cardDuplicateInstance)
		
		
		UI_MANAGER.DuplicateAnimationCard(GAME_MANAGER.GetActiveCard())

func Undo_Prep():
	$Area2D.set_collision_layer_value(1,true)
	$".".set_collision_layer_value(1,true)
	Touched = true
