extends Floor

class_name PreparationFloor

const SHAKE_FREQUENCY = 50
const SHAKE_AMPLITUDE = 40

var time : float

func _ready() -> void:
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
	Sprite = %Sprite2D
	Sprite.texture = InitialWhiteSprite
	GAME_MANAGER.InicioGameStage.connect(PutInGame)

func _physics_process(delta: float) -> void:
	_Shake(delta)

func _on_area_2d_body_entered(body):
	if !Touched:
		if body.is_in_group("Player"):
			Touch(body)
			
func _on_area_2d_body_exited(body: Node2D) -> void:
	if Touched:
		FloorAnimator.play("Destroy")
		await  FloorAnimator.animation_finished
		queue_free()
		#hide()
		#$Area2D.set_collision_layer_value(1,false)
		#$".".set_collision_layer_value(1,false)
			
func Touch(body):
	Touched = true
	Sprite.texture = TouchedSprite
	GAME_MANAGER.ConvertFloors.append($".")

func Restart():
	Touched = false
	Sprite.texture = InitialWhiteSprite
	SHEET_MANAGER.metadata_BlocksTouched -= 1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func _Shake(delta):
	if Touched:
		time += delta
		var movement = cos(time *  SHAKE_FREQUENCY) * SHAKE_AMPLITUDE
		Sprite.position.x += movement * delta
		Sprite.position.y += movement * delta
func _Destroy():
	FloorAnimator.play("Destroy")
	await  FloorAnimator.animation_finished
	queue_free()

func PutInGame():
	_Destroy() 
