extends PreparationFloor

@export var ColorCard : Floor.ColorEnum

func Touch(body):
	super(body)
	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		
		GAME_MANAGER.GetActiveCard().ColorFloor = ColorCard
		GAME_MANAGER.GetActiveCard().ShowCard()
		GAME_MANAGER.CambioCarta.emit()
		
		#for card in GAME_MANAGER.Deck:
			#UI_MANAGER.PutCard(card)
			
		#if GAME_MANAGER.GetCardsOnDeck().size() > 0:
			#GAME_MANAGER.GetActiveCard().animation_player.play("Shake")

func Undo_Prep():
	$Area2D.set_collision_layer_value(1,true)
	$".".set_collision_layer_value(1,true)
	Touched = true
