extends PreparationFloor

@export var ColorCard : Floor.ColorEnum

func Touch(body):
	super(body)
	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		
		var cardSelectedAux : bool = true
		for card in GAME_MANAGER.Deck:
			if card.ColorFloor == ColorCard and cardSelectedAux:
				UI_MANAGER.PutAsFirstCard(card)
				
				GAME_MANAGER.Deck.erase(card)
				GAME_MANAGER.Deck.push_front(card)
				cardSelectedAux = false
		if !cardSelectedAux:
			UI_MANAGER.ShakeActiveCard()
		

func Undo_Prep():
	$Area2D.set_collision_layer_value(1,true)
	$".".set_collision_layer_value(1,true)
	Touched = true
