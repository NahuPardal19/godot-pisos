extends PreparationFloor

func Touch(body):
	super(body)
	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		GAME_MANAGER.GetActiveCard().OnShake = false
		UI_MANAGER.ReverseDeck()
		GAME_MANAGER.Deck.reverse()
		
		await get_tree().create_timer(0.3).timeout
	
		UI_MANAGER.ShakeActiveCard()

func Undo_Prep():
	Respawn()
	GAME_MANAGER.GetActiveCard().animation_player.stop()
	GAME_MANAGER.Deck.reverse()
	for card in GAME_MANAGER.Deck:
		UI_MANAGER.PutCard(card)
	if GAME_MANAGER.GetCardsOnDeck().size() > 0:
		GAME_MANAGER.GetActiveCard().animation_player.play("Shake")
	Touched = false

