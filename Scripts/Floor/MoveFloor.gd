extends Floor

enum DirectionEnum{Left,Right,Down,Up}
@export var Direction : DirectionEnum  
var initial_position

@onready var ray_cast_up: RayCast2D = $RayCasts/RayCastUp
@onready var rat_cast_down: RayCast2D = $RayCasts/RatCastDown
@onready var ray_cast_right: RayCast2D = $RayCasts/RayCastRight
@onready var ray_cast_left: RayCast2D = $RayCasts/RayCastLeft

@onready var sprite_2d_2: Sprite2D = $Sprite2D2


func _ready():
	if !GAME_MANAGER.StageEnum.LevelEditorStage:
		ray_cast_up.add_exception(GAME_MANAGER.PlayerList[0])
		ray_cast_right.add_exception(GAME_MANAGER.PlayerList[0])
		ray_cast_left.add_exception(GAME_MANAGER.PlayerList[0])
		rat_cast_down.add_exception(GAME_MANAGER.PlayerList[0])
	
	AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
	Sprite = %Sprite2D
	super()
	
	initial_position = position
	sprite_2d_2.hide()

				
func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		AudioPlayer.stream = AUDIO_MANAGER.Floor_TouchSound
		
	super(body)
	
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		$Sprite2D2/Area2D.set_collision_layer_value(2,true)
		if Direction == DirectionEnum.Left:
			if !ray_cast_left.is_colliding():
				sprite_2d_2.show()
		if Direction == DirectionEnum.Right:
			if !ray_cast_right.is_colliding():
				sprite_2d_2.show()
		if Direction == DirectionEnum.Up:
			if !ray_cast_up.is_colliding():
				sprite_2d_2.show()
		if Direction == DirectionEnum.Down:
			if !rat_cast_down.is_colliding():
				sprite_2d_2.show()
func TouchWithConnector(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game:
		super(body)
		
		$Sprite2D2/Area2D.set_collision_layer_value(2,true)
		if Direction == DirectionEnum.Left:
				if !ray_cast_left.is_colliding():
					sprite_2d_2.show()
		if Direction == DirectionEnum.Right:
				if !ray_cast_right.is_colliding():
					sprite_2d_2.show()
		if Direction == DirectionEnum.Up:
				if !ray_cast_up.is_colliding():
					sprite_2d_2.show()
		if Direction == DirectionEnum.Down:
				if !rat_cast_down.is_colliding():
					sprite_2d_2.show()
			

func Activate():
	$Sprite2D2/Area2D.set_collision_layer_value(2,false)
	Move()

func Restart():
	super()
	sprite_2d_2.hide()
	SHEET_MANAGER.metadata_BlocksTouched -= 1
	position = initial_position

func Move():
	Restart()
	if Direction == DirectionEnum.Left:
		if !ray_cast_left.is_colliding():
			global_position += Vector2.LEFT*40
			if connector != null:
				connector.global_position += Vector2.LEFT*40
	if Direction == DirectionEnum.Right:
		if !ray_cast_right.is_colliding():
			global_position += Vector2.RIGHT*40
			if connector != null:
				connector.global_position += Vector2.RIGHT*40
	if Direction == DirectionEnum.Up:
		if !ray_cast_up.is_colliding():
			global_position += Vector2.UP*40
			if connector != null:
				connector.global_position += Vector2.UP*40
	if Direction == DirectionEnum.Down:
		if !rat_cast_down.is_colliding():
			global_position += Vector2.DOWN*40
			if connector != null:
				connector.global_position += Vector2.DOWN*40
