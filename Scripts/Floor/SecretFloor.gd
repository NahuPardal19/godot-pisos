extends Floor

@onready var CollisionCube:CollisionShape2D = $CollisionShape2D
@onready var CollisionArea:CollisionShape2D = $Area2D/CollisionShape2D

@onready var audio_stream_player_2d = $AudioStreamPlayer2D

func _ready():
	audio_stream_player_2d.stream = AUDIO_MANAGER.FloorSound
	Sprite = $Sprite2D

func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		Touch(body)

func Activate():
	if Touched:
		if is_instance_valid(CollisionCube) and is_instance_valid(CollisionArea):
			CollisionArea.queue_free()
			CollisionCube.queue_free()
	Sprite.texture = InitialWhiteSprite
	
func Touch(body):
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Preparation:
		if connector != null:
			connector.TouchFloors(body)
			return
		if !Touched:
			GAME_MANAGER.AgregarPiso(get_node("."))
			Touched = true
			Sprite.texture = TouchedSprite
			audio_stream_player_2d.play()

func Restart():
	Touched = false
	Sprite.texture = InitialWhiteSprite
