extends Node

#GENERAL

#SINGLEPLAYER
var TextoRondas
var Lifes
var Undo
var Menu : Control
var Background 
var Cards

var Cards_offset_x = 110.0

func _ready():
	pass 

func _process(_delta):
	pass
	#if Input.is_action_just_pressed("Player1_Jump"):
		#_MoveCards()

func PutTextRound(rounds):
	TextoRondas.get_child(0).text = str(rounds)
func PutLife(life):
	Lifes.get_child(0).text = str(life)
func PutUndo(undoAmount):
	Undo.get_child(0).text = str(undoAmount)
func PutMenu(state : bool):
	Menu.visible = state
func PutCard(CardToPut:Card):
	CardToPut.get_parent().remove_child(CardToPut)
	Cards.add_child(CardToPut)
func ChangebackgroundToGame():
	Background.ChangeToGame()
func ChangebackgroundToPreparation():
	Background.ChangeToPreparation()
func QuitLife():
	Lifes.get_child(1).play("Hearth break")
	await Lifes.get_child(1).animation_finished
	Lifes.get_child(1).frame = 0

func DrawCards():
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
	
	var screen_size = get_viewport().get_visible_rect().size
	var screen_center_x = screen_size.x / 2.0
	
	var deck_size = GAME_MANAGER.Deck.size()
	GAME_MANAGER.StopPlayer(false,GAME_MANAGER.PlayerList[0])
	
	for card in GAME_MANAGER.Deck:
		var index = GAME_MANAGER.Deck.find(card)
		var final_position : Vector2 = Vector2()
				
		final_position.x = screen_center_x + (Cards_offset_x * (index - (deck_size - 1) / 2.0))
		final_position.y = card.global_position.y  
		
		var rot_max = -0.2
		
		var _rot_radians:float = lerp_angle(-rot_max,rot_max , float(index)/float(GAME_MANAGER.Deck.size()-1))
		
		tween.parallel().tween_property(card, "global_position", final_position, 1 + (index * 0.075))
		#tween.parallel().tween_property(card,"rotation" , rot_radians , 1 + (index * 0.075))
		
	await tween.finished
	if GAME_MANAGER.PlayerList.size() > 0:
		GAME_MANAGER.StopPlayer(true,GAME_MANAGER.PlayerList[0])
	UI_MANAGER.ShakeActiveCard()
func ReverseDeck():
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
	var deck_normal = []
	var deck_reversed = []
	for card in GAME_MANAGER.Deck:
		if card.Actived:
			deck_reversed.append(card)
			deck_normal.append(card)
	
	deck_reversed.reverse()
	
	for i in range(deck_normal.size()):
		
		var final_position = Vector2()
		final_position.x = deck_reversed[i].global_position.x
		final_position.y = deck_normal[i].initial_y_position
		deck_normal[i].rotation_degrees = 0
		tween.parallel().tween_property(deck_normal[i], "global_position", final_position, .8)
	await tween.finished
	center_cards()
func DuplicateAnimationCard(card):
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
	var final_position = Vector2()
	final_position.x = card.global_position.x - Cards_offset_x
	final_position.y = card.initial_y_position
	card.rotation_degrees = 0
	tween.tween_property(card, "global_position", final_position, .3)
	await tween.finished
	center_cards()
	ShakeActiveCard()
		
func center_cards():
	# Obtener el tamaño de la pantalla
	var screen_size = get_viewport().get_visible_rect().size
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)

	# Calcular el ancho total de las cartas
	var total_width = 0
	for card in GAME_MANAGER.GetCardsOnDeck():
		total_width += card.size.x

	# Calcular la posición inicial para centrar todas las cartas
	var current_x = (screen_size.x - total_width - Cards_offset_x * (GAME_MANAGER.GetCardsOnDeck().size() - 1)) / 2

	# Animar las cartas a sus nuevas posiciones centradas
	for card in GAME_MANAGER.GetCardsOnDeck():
		var target_position = Vector2(current_x, card.global_position.y)
		tween.parallel().tween_property(card, "global_position", target_position, .3)
		current_x +=  Cards_offset_x

		
func ShakeActiveCard():
	for card in GAME_MANAGER.Deck:
		card.rotation_degrees = 0
		card.OnShake = false
	if GAME_MANAGER.GetActiveCard() != null:
		GAME_MANAGER.GetActiveCard().OnShake = true

func _MoveCards():
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
	var ActiveToMove:bool = false
	
	var active_deck = []
	for card in GAME_MANAGER.Deck:
		if card.Actived:
			active_deck.append(card)
			
	for i in range(active_deck.size() - 1, -1, -1):
		var distanceWithNextCard = abs(GAME_MANAGER.Deck[i].global_position.x-GAME_MANAGER.Deck[i-1].global_position.x)
		if ActiveToMove:
			var final_position : Vector2 = Vector2()
			final_position.x =active_deck[i].global_position.x + Cards_offset_x
			final_position.y = active_deck[i].initial_y_position
			tween.parallel().tween_property(active_deck[i], "global_position",final_position,.3)
		else:
			if distanceWithNextCard > 150:
				ActiveToMove = true
	await tween.finished
	UI_MANAGER.ShakeActiveCard()
func PutAsFirstCard(card):
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
	var final_position : Vector2 = Vector2()
	final_position.x = GAME_MANAGER.GetActiveCard().global_position.x - Cards_offset_x
	final_position.y = card.global_position.y  
	card.set_z_index(10)
	tween.tween_property(card, "global_position",final_position,.6)
	await tween.finished
	center_cards()
