extends Node

const SAVEFILE = "user://SAVEFILE.save"

var GameData ={
	"Level" : 0
}

func _ready() -> void:
	load_data()	
	
func load_data():
	var file = FileAccess.open(SAVEFILE,FileAccess.READ)
	if file == null:
		save_data()
	else:
		GameData = file.get_var()
	file = null
	
func save_data():
	var file = FileAccess.open(SAVEFILE,FileAccess.WRITE)
	file.store_var(GameData)
	file = null
