extends Node

var AudioPlayer : AudioStreamPlayer
const NormalVolume : float = 2

#-Floor SOUNDS
@onready var Floor_TouchSound : AudioStream = ResourceLoader.load("res://Audio/SFX/Floor/Floor.wav") as AudioStream
#@onready var Floor_Destroy : AudioStream = ResourceLoader.load() as AudioStream
@onready var Floor_JumpBoost : AudioStream = ResourceLoader.load("res://Audio/SFX/Floor/JumpBoost.wav") as AudioStream
#
##UI SOUNDS
@onready var SelectedButton : AudioStream = ResourceLoader.load("res://Audio/SFX/UI/Button Selected.wav") as AudioStream
@onready var PressedButton : AudioStream = ResourceLoader.load("res://Audio/SFX/UI/Button Pressed.wav") as AudioStream
@onready var FadeInSound : AudioStream = ResourceLoader.load("res://Audio/SFX/UI/FadeInSound.wav") as AudioStream
@onready var FadeOutSound : AudioStream = ResourceLoader.load("res://Audio/SFX/UI/FadeOutSound.wav") as AudioStream
@onready var Typing : AudioStream = ResourceLoader.load("res://Audio/SFX/UI/Typing.wav") as AudioStream


@onready var Player_Jump : AudioStream = ResourceLoader.load("res://Audio/SFX/Player/jump.mp3") as AudioStream
@onready var Player_LifeQuit : AudioStream = ResourceLoader.load("res://Audio/SFX/Player/lose_health.mp3") as AudioStream
@onready var PLayer_Undo : AudioStream = ResourceLoader.load("res://Audio/SFX/Player/undo1.mp3") as AudioStream
@onready var Player_RoundPassed : AudioStream = ResourceLoader.load("res://Audio/SFX/Player/Player_RoundPassed.wav") as AudioStream
@onready var Player_DeadFall : AudioStream = ResourceLoader.load("res://Audio/SFX/Player/deathFall.mp3") as AudioStream
@onready var Player_DeadNormal : AudioStream = ResourceLoader.load("res://Audio/SFX/Player/death1.mp3") as AudioStream

@onready var Card_Disolve : AudioStream = ResourceLoader.load("res://Audio/SFX/Card/CardDisolve.mp3") as AudioStream

##Music Sounds
enum MusicEnum {Menu,Employee_Music,Clown_Music}
@onready var MenuMusic : AudioStream = ResourceLoader.load("res://Audio/Music/Music1.mp3") as AudioStream
@onready var EmployeeMusic : AudioStream = ResourceLoader.load("res://Audio/Music/EmployeeMusic.mp3") as AudioStream
@onready var ClownMusic : AudioStream = ResourceLoader.load("res://Audio/Music/ClownMusic.mp3") as AudioStream

func _ready() -> void:
	AudioPlayer = AudioStreamPlayer.new()
	self.add_child(AudioPlayer)
	AudioPlayer.bus = "Music"
	
	
func PlayMusic(music_typeP : MusicEnum , duration : float , volume):
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
	AudioPlayer.volume_db = -10
	if music_typeP == MusicEnum.Menu:
		AudioPlayer.stream = MenuMusic
		AudioPlayer.play()
		tween.tween_property(AudioPlayer,"volume_db",volume,duration)
	elif music_typeP == MusicEnum.Employee_Music:
		AudioPlayer.stream = EmployeeMusic
		AudioPlayer.play()
		tween.tween_property(AudioPlayer,"volume_db",volume,duration)
	elif music_typeP == MusicEnum.Clown_Music:
		AudioPlayer.stream = ClownMusic
		AudioPlayer.play()
		tween.tween_property(AudioPlayer,"volume_db",volume,duration)
		
func TransitMusic(music_typeP : MusicEnum, durationFadeOut , durationFadeIn):
	FadeOutMusic(durationFadeOut)
	await get_tree().create_timer(durationFadeOut).timeout
	PlayMusic(music_typeP,durationFadeIn,NormalVolume)

func FadeOutMusic(duration):
	var tween : Tween = get_tree().create_tween().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_LINEAR)
	tween.tween_property(AudioPlayer,"volume_db",-50,duration)
	#await tween.finished
	#StopMusic()

func StopMusic():
	AudioPlayer.stop()

func Create_AudioPlayer() -> AudioStreamPlayer:
	var AudioPlayer = AudioStreamPlayer.new()
	self.add_child(AudioPlayer)
	AudioPlayer.bus = "Music"
	return AudioPlayer
