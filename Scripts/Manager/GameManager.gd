extends Node2D

var rnd = RandomNumberGenerator.new()

enum StageEnum {Preparation , Game , Menu , LevelEditorStage}
var Stage : StageEnum

const Max_Level = 18

#region General_Atributtes
@export_group("General Atributtes")
var InitOnRestart : bool
var RestartOnStage : StageEnum

var ConvertFloors : Array[Floor] = []
var FloorsToActivate : Array[Floor]= []
var LastFloorTouched : Floor

var PosicionInicial : Node2D 
var Map : TileMap

var InitialSpawner
var InitialSpawn

var BlocksLevelEdited : Array[Floor] = []
var Transitioner:Transitioner
var PlayerList : Array[Player] = []

var Spawner : Floor 

##DATOS
var LevelEdited : bool
var RondasObjetivo : int
var RondasPasadas : int = 0
var UndoAmount : int = 5
var LevelsLifes : int

##BOOLS DE CONTROL
var CanUndo : bool = false
var OnUndo : bool = false
var CanRestart : bool = true
var OnRestart : bool = false
var CanPause : bool = true
var MenuOnEditor : bool = false

var SiguienteNivelPath : String
var AnteriorNivelPath : String

##UI
var OnController : bool

var PlayedTime:float

@export_group("Cards")
var Deck : Array[Card] = []

##EVENTOS
signal InicioPartida
signal PasoRonda
signal FinPartida

signal InicioPreparationStage
signal InicioGameStage

signal CambioCarta

##LEVEL EDITOR
var Level_Loaded : bool = false
var _File_To_Load : String

var MapName : String
var IsOnline : bool
var OnLevelEditorMode : bool = false
var Online_Level = []
##MENU
var Menu_Map_File : String
var CantEdit : bool 

func _ready() -> void:
	Stage = StageEnum.Preparation
	SHEET_MANAGER.id = 0
	if Engine.has_singleton("JavaScript"):
		var js = Engine.get_singleton("JavaScript")
		js.eval("window.Godot = { onBeforeUnload: function() { return %s.onBeforeUnload(); } }" % get_instance_id())
	get_tree().set_auto_accept_quit(false)
	
#func _process(delta: float) -> void:
	#print(OnRestart)
#region Singleplayer_Methods
func IniciarPartida():
	DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_HIDDEN)
	CanRestart = false
	CanPause = false
	InicioPartida.emit()
	PlayerList[0].position = PosicionInicial.global_position
	InitialSpawn = PosicionInicial
	UI_MANAGER.PutLife(PlayerList[0].Lifes)
	UndoAmount = 5
	
	if !CanUndo:
		UI_MANAGER.Undo.hide()
	
	if Deck.size() <= 0 :
			InitGameStage(true)
			LevelEdited = false
	else: InitPreparationStage()
	

	
	if InitOnRestart:
		Transitioner._RestartAnimation(true)
		#Transitioner.color_rect_down.visible = true
		#Transitioner.color_rect_up.visible = true
		Transitioner.color_rect.visible = false
		StopPlayer(false , PlayerList[0])
	
		await Transitioner.animation_player.animation_finished
		if Stage == StageEnum.Game:
			StopPlayer(true , PlayerList[0])	
	else:
		#Transitioner.color_rect.visible = true
		StopPlayer(false, PlayerList[0])
		Transitioner._LevelAnimation(false)
		await Transitioner.animation_player.animation_finished
		if Stage == StageEnum.Game:
			StopPlayer(true, PlayerList[0])
	CanRestart = true
	CanPause = true
func Loose():
	StopPlayer(false,PlayerList[0])
	if PlayerList[0].Sprite.flip_h:
		PlayerList[0].Sprite.position += Vector2(-11,0)
	else:
		PlayerList[0].Sprite.position += Vector2(11,0)
	PlayerList[0].animation_player.play("Die")
	PlayerList[0].audio_stream_player_2d.stream = AUDIO_MANAGER.Player_DeadNormal
	PlayerList[0].audio_stream_player_2d.play()
	await PlayerList[0].animation_player.animation_finished
	Restart()
func PasarRonda(body):
	RondasPasadas += 1
	
	if RondasPasadas >= RondasObjetivo:
		RondasPasadas = 0
		Win()
		return
	
	StopPlayer(false , PlayerList[0])
	
	for floorP in FloorsToActivate:
		if is_instance_valid(floorP):
			floorP.Activate()
	FloorsToActivate.clear()	
	
	CanRestart = false
	PlayerList[0].animation_player.play("Teleport_Out")
	await PlayerList[0].animation_player.animation_finished

	UI_MANAGER.PutTextRound(RondasObjetivo-RondasPasadas)
	PasoRonda.emit()
	if !OnRestart:
		body.position = ReturnSpawn().spawn.global_position
	PlayerList[0].animation_player.play("Teleport_In")
	await PlayerList[0].animation_player.animation_finished
	for floorp in FloorsToActivate:
		if is_instance_valid(floorp):
			floorp.Activate()
	CanRestart = true
	CanPause = true
	StopPlayer(true , PlayerList[0])
func Win():
	StopPlayer(false, PlayerList[0])
	Transitioner._LevelAnimation(true)
	await Transitioner.animation_player.animation_finished
	StopPlayer(true, PlayerList[0])
	InitOnRestart=false
	NextLevel()
	FinPartida.emit()
	SHEET_MANAGER.metadata_RemainingLives = PlayerList[0].Lifes
	SHEET_MANAGER.UpdateMetadata()
	print(SHEET_MANAGER.metadata)
func Restart():
	if CanRestart:
		OnRestart = true
		if LevelEdited:
			CanRestart = false
			CanPause = false
			StopPlayer(false, PlayerList[0])
			Transitioner._RestartAnimation(false)
			await Transitioner.animation_player.animation_finished
			PosicionInicial = InitialSpawn
			InitialSpawner.Active = true
			UsePreviousLevel()
			#InitialSpawner._init_sprite()
			PlayerList[0].animation_player.stop()
			PlayerList[0].OnDie = false
			PlayerList[0].position = PosicionInicial.global_position
			PlayerList[0].scale = Vector2(1,1)
			PlayerList[0].Sprite.flip_h = false
			StopPlayer(true, PlayerList[0])
			if Stage == StageEnum.Preparation:
				RestartOnStage = StageEnum.Preparation
			else:
				RestartOnStage = StageEnum.Game
			InitOnRestart = true
			RondasPasadas = 0
			ChangeFloor.ChangeFloors.clear()
			FloorsToActivate.clear()
			ConvertFloors.clear()
			LastFloorTouched = null
			PlayerList[0].Lifes = LevelsLifes
			UI_MANAGER.PutLife(LevelsLifes)
			UI_MANAGER.PutTextRound(str(RondasObjetivo))
			
			Transitioner.color_rect_down.visible = true
			Transitioner.color_rect_up.visible = true
			#Transitioner.color_rect.visible = false
			Transitioner._RestartAnimation(true)
			await Transitioner.animation_player.animation_finished
			
			StopPlayer(true , PlayerList[0])	
			CanRestart = true
			CanPause = true
		else:
			Transitioner._RestartAnimation(false)
			CanRestart = false
			CanPause = false
			StopPlayer(false, PlayerList[0])
			await Transitioner.animation_player.animation_finished
			FinPartida.emit()
			StopPlayer(true, PlayerList[0])
			InitOnRestart = true
			if Stage == StageEnum.Preparation:
				RestartOnStage = StageEnum.Preparation
			else:
				RestartOnStage = StageEnum.Game
			get_tree().reload_current_scene()
			RondasPasadas = 0
			SHEET_MANAGER.RestartMetadata(false)
		OnRestart = false

func InitPreparationStage():
	PlayerList[0].Sprite.texture = load("res://Sprites/Others/Personaje Preparation.png")
	Stage = StageEnum.Preparation
	UI_MANAGER.ChangebackgroundToPreparation()
	LevelEdited = false
	if RestartOnStage != StageEnum.Preparation:
		AUDIO_MANAGER.TransitMusic(AUDIO_MANAGER.MusicEnum.Clown_Music,1,2)
	
	InicioPreparationStage.emit()
	RestartOnStage =  StageEnum.Preparation
	await Transitioner.animation_player.animation_finished
	UI_MANAGER.DrawCards()
func InitGameStage(OnInit:bool):
	Stage = StageEnum.Game
	UI_MANAGER.Lifes.get_child(0).show()
	UI_MANAGER.Lifes.get_child(1).show()
	UI_MANAGER.TextoRondas.get_child(0).show()
	UI_MANAGER.TextoRondas.get_child(1).show()
	
	if RestartOnStage != StageEnum.Game:
		AUDIO_MANAGER.TransitMusic(AUDIO_MANAGER.MusicEnum.Employee_Music,2,3.5)
	
	CanRestart = false
	CanPause = false
	UI_MANAGER.Cards.hide()
	
	
	UndoAmount = 5
	UI_MANAGER.PutUndo(UndoAmount)
	
	StopPlayer(false , PlayerList[0])
	#UI_MANAGER.ChangebackgroundToGame()
	
	if !OnInit:
		PlayerList[0].animation_player.play("Clown_Dissapear")
		await PlayerList[0].animation_player.animation_finished
		
	
	UI_MANAGER.ChangebackgroundToGame()
	InicioGameStage.emit()
	
	PlayerList[0].Sprite.texture = load("res://Sprites/Others/Personaje.png")
	
	PlayerList[0].position = PosicionInicial.global_position
	PlayerList[0].Sprite.flip_h = false
	
	if !OnInit:
		PlayerList[0].animation_player.play("Appear")
		await PlayerList[0].animation_player.animation_finished
	else:
		PlayerList[0].animation_player.play("idle")
	
	if !OnInit:
		LevelEdited = true
	
	StopPlayer(true , PlayerList[0])
	CanPause = true
	CanRestart = true
	
	
	#CanUndo = true

func UsePreviousLevel():
	for block in Map.get_children():
		block.Restart()

func GetActiveCard() -> Card:
	
	if Deck.size() > 0:
		for card in Deck:
			if card.Actived == true:
				return card
	return null
func GetCardsOnDeck() -> Array[Card]:	
	var array : Array[Card] = []
	if Deck.size() > 0:
		for card in Deck:
			if card.Actived == true:
				array.append(card)
	return array
func GetFirstCard() -> Card:
	if GetCardsOnDeck().size() > 0:
		var card : Card = Deck[0]
		return card
	else: return null
func GetLastCard() -> Card:
	if GetCardsOnDeck().size() > 0:
		var card:Card = Deck[Deck.size()-1]
		return card
	else: return null
func QuitCard(card:Card):
	card.Destroy()
	CambioCarta.emit()
	if GetCardsOnDeck().size() <= 0:
		CanUndo = false
		AUDIO_MANAGER.TransitMusic(AUDIO_MANAGER.MusicEnum.Employee_Music,2,3.5)
		await card.animation_player.animation_finished
		#PlayerList[0].animation_player.play("Clown_Dissapear")
		CanPause = false
		StopPlayer(false,PlayerList[0])
		Deck.clear()
		await PlayerList[0].animation_player.animation_finished
		
		InitGameStage(false)
	else:
		await card.animation_player.animation_finished
		UI_MANAGER.center_cards()
		UI_MANAGER.ShakeActiveCard()

func _notification(what) -> void:
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		if !OS.has_feature("editor"):
			SHEET_MANAGER.Add()
			await SHEET_MANAGER.http.request_completed
			get_tree().quit()
		else:
			get_tree().quit()
func onBeforeUnload():
	SHEET_MANAGER.Add()
	await SHEET_MANAGER.http.request_completed
	return "done"

func AddLastCard():
	for i in range(Deck.size()-1,-1,-1):
		if Deck[i].Actived == false:
			GetActiveCard().animation_player.stop()
			Deck[i].Active()
			GetActiveCard().animation_player.play("Shake")
			CambioCarta.emit()
			return
#endregion


func LoadLevelEditorMap(file):
	_File_To_Load = file
	get_tree().change_scene_to_file("res://Scenes/Level Editor/MapLoader.tscn")

func InitializeLevelEditorMap(RoundsP:int,LifesP:int,LevelP,CardsP,Tilemap,CardsPlace):
	#RONDAS
	RondasObjetivo = RoundsP
	UI_MANAGER.PutTextRound(str(RondasObjetivo))
	#CARDS
	Deck.clear()
	if Online_Level:
		for card in CardsP:
			var card_path = card["mapValue"]["fields"]["filePath"]["stringValue"]
			var card_color = card["mapValue"]["fields"]["color"]["stringValue"]
			var card_loaded = load(card_path)
			if card_loaded != null:
				var card_instance = card_loaded.instantiate()
				if card_color == "white":
					card_instance.ColorFloor = Floor.ColorEnum.White
				elif card_color == "yellow":
					card_instance.ColorFloor = Floor.ColorEnum.Yellow
				elif card_color == "green":
					card_instance.ColorFloor = Floor.ColorEnum.Green
				CardsPlace.add_child(card_instance)
				Deck.append(card_instance)
				
		for block in LevelP:
			var block_path = block["mapValue"]["fields"]["filePath"]["stringValue"]
			var block_position = block["mapValue"]["fields"]["position"]["mapValue"]["fields"]
			var blockLoaded = load(block_path)
			if blockLoaded != null:
				var block_instance = blockLoaded.instantiate()
				block_instance.position = Vector2(int(block_position["x"]["integerValue"]),int(block_position["y"]["integerValue"]))
				Tilemap.add_child(block_instance)
	else:
		for card in CardsP:
			var card_path = card["filePath"]
			var card_color = card["color"]
			var card_loaded = load(card_path)
			if card_loaded != null:
				var card_instance = card_loaded.instantiate()
				if card_color == "white":
					card_instance.ColorFloor = Floor.ColorEnum.White
				elif card_color == "yellow":
					card_instance.ColorFloor = Floor.ColorEnum.Yellow
				elif card_color == "green":
					card_instance.ColorFloor = Floor.ColorEnum.Green
				CardsPlace.add_child(card_instance)
				Deck.append(card_instance)
				
			#MAP
		for block in LevelP:
			var block_path = block["filePath"]
			var block_position = block["position"]
			var blockLoaded = load(block_path)
			if blockLoaded != null:
				var block_instance = blockLoaded.instantiate()
				block_instance.position = _string_to_vector2(block_position)
				Tilemap.add_child(block_instance)
	#LIFES
	LevelsLifes = LifesP
	#Level_Loaded = true
	

func _string_to_vector2(string := "") -> Vector2:
	if string:
		var new_string:String = string
		new_string = new_string.erase(0,1)
		new_string = new_string.erase(new_string.length()-1,1)
		var array: Array = new_string.split(", ")
		
		return Vector2(int(array[0]),int(array[1]))
	return Vector2.ZERO
func ReturnSpawn():
	for block in Map.get_children():
		if block.scene_file_path == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn" or block.scene_file_path == "res://Scenes/Floors/Game Floors/SpawnerDesactivated.tscn":
			if block.Active:
				return block
	return null

func GoToMenu():
	Engine.time_scale = 1
	InitOnRestart = false
	get_tree().change_scene_to_file("res://Scenes/Levels/menu.tscn")
func AgregarPiso(piso):
	LastFloorTouched = piso
	FloorsToActivate.append(piso)
func Pause():
	if UI_MANAGER.Menu.visible == false and CanPause and !UI_MANAGER.Menu.visible:
		DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_VISIBLE)
		UI_MANAGER.PutMenu(true)
		StopPlayer(false , PlayerList[0])
	else: 
		if CanPause and UI_MANAGER.Menu.visible == true:
			UI_MANAGER.PutMenu(false)
			DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_HIDDEN)
			StopPlayer(true , PlayerList[0])
func StopPlayer(movement:bool , playerP):
	if playerP != null:
		playerP.CanPlay = movement
func Undo():
	
	if UndoAmount > 0 and CanUndo:
		OnUndo = true 
		if Stage == StageEnum.Game:
			if FloorsToActivate.size() > 1:
				
				SHEET_MANAGER.metadata_Undo += 1
				UndoAmount-=1
				PlayerList[0].audio_stream_player_2d.stream = AUDIO_MANAGER.Undo
				PlayerList[0].audio_stream_player_2d.play()
				
				if FloorsToActivate[FloorsToActivate.size()-1].connector != null:
					FloorsToActivate[FloorsToActivate.size()-1].connector.RestartConnectors()
					if FloorsToActivate.size()>0:
						PlayerList[0].global_position = FloorsToActivate[FloorsToActivate.size()-1].global_position + Vector2.UP*26
					else:
						PlayerList[0].global_position = PosicionInicial.global_position
				else:
					PlayerList[0].audio_stream_player_2d.stream = AUDIO_MANAGER.Undo
					PlayerList[0].audio_stream_player_2d.play()
					
					PlayerList[0].global_position = FloorsToActivate[FloorsToActivate.size()-2].global_position + Vector2.UP*26			
					FloorsToActivate[FloorsToActivate.size()-1].Restart()
					FloorsToActivate.erase(FloorsToActivate[FloorsToActivate.size()-1])
			elif  FloorsToActivate.size() == 1:
				PlayerList[0].audio_stream_player_2d.stream = AUDIO_MANAGER.Undo
				PlayerList[0].audio_stream_player_2d.play()
				PlayerList[0].global_position = PosicionInicial.global_position
				UndoAmount-=1
				SHEET_MANAGER.metadata_Undo += 1
				if FloorsToActivate.size()>0:
					FloorsToActivate[0].Restart()
					FloorsToActivate.erase(FloorsToActivate[0])
			UI_MANAGER.PutUndo(UndoAmount)
		else:
			if ConvertFloors.size() > 1:
				UndoAmount-=1
				SHEET_MANAGER.metadata_Undo += 1
				PlayerList[0].audio_stream_player_2d.stream = AUDIO_MANAGER.Undo
				PlayerList[0].audio_stream_player_2d.play()
				PlayerList[0].global_position = ConvertFloors[ConvertFloors.size()-2].global_position + Vector2.UP*30
				ConvertFloors[ConvertFloors.size()-2].Respawn()
				
				await get_tree().create_timer(0.1).timeout
				AddLastCard()
				ConvertFloors[ConvertFloors.size()-1].Undo_Prep()
				ConvertFloors.erase(ConvertFloors[ConvertFloors.size()-1])
			else:
				SHEET_MANAGER.metadata_Undo += 1
				UndoAmount-=1
				PlayerList[0].audio_stream_player_2d.stream = AUDIO_MANAGER.Undo
				PlayerList[0].audio_stream_player_2d.play()
				PlayerList[0].global_position = PosicionInicial.global_position
				await get_tree().create_timer(0.1).timeout
				AddLastCard()
				ConvertFloors[ConvertFloors.size()-1].Undo_Prep()
				ConvertFloors.erase(ConvertFloors[0])
				UI_MANAGER.PutUndo(UndoAmount)
		OnUndo = false
		
func NextLevel():
	FinPartida.emit()
	InitOnRestart = false
	if GAME_MANAGER.OnLevelEditorMode:
		LevelEditor.LevelToEdit = true
	get_tree().change_scene_to_file(SiguienteNivelPath)
func PreviousLevel():
	FinPartida.emit()
	InitOnRestart = false
	get_tree().change_scene_to_file(AnteriorNivelPath)
