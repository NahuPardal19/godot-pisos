extends Node

# id = entry.2100534782

# time played = entry.1019368948

# metadata = entry.452041058
#{"painting_0":{"TimePlayed":0,"Deads":4,"Blocks_Touched":11899,"Remaining_Lives ":5400,"Undo":3,"Is_Solved":true}
#
var id

var time_played = 0
var time = 0

var metadata = "{"
var metadata_Level : String
var metadata_LevelTimePlayed : float
var metadata_Attemps : int
var metadata_BlocksTouched : int
var metadata_RemainingLives : int
var metadata_Undo : int

var first_request:bool = true

const url_data = "https://opensheet.elk.sh/1YcYXfrHFbT7aftG6TWxEANoR8YdzDbPIG6Vbgpd40j8/Forms%20data"
const url_submit = "https://docs.google.com/forms/u/0/d/e/1FAIpQLScpDMWhqbVXKjLLH5zTITs1ZnNNQ2QgmO6vlQ66_URFrSRcBg/formResponse"
const headers = ["Content-Type: application/x-www-form-urlencoded"]

var client = HTTPClient.new()
var http : HTTPRequest

func _process(delta: float) -> void:
	time += 1*delta
	

func http_submit(_result, _response_code, _headers, _body):
	http.queue_free()

func http_done(_result, _response_code, _headers, _body):
	http.queue_free()	
	
	

func Add():
	http = HTTPRequest.new()
	http.request_completed.connect(http_submit)
	add_child(http)
	
	time_played = str("%0.01f"%time)
	metadata += "}"  
	
	var user_data = client.query_string_from_dict({
	"entry.2100534782": id,
	"entry.1019368948" : time_played,
	"entry.452041058" : metadata
	})

	#var user_data = "entry.2100534782=2&entry.1019368948=20055&entry.452041058=Viktor"
	var error = http.request(url_submit, headers, HTTPClient.METHOD_POST, user_data)
	
	if error != OK:
		print("Error: %d" % error)
		http.queue_free()
	else:
		print("Request sent")
func Update():
	http = HTTPRequest.new()
	http.request_completed.connect(http_done)
	add_child(http)
	
	#var user_data = "entry.2100534782=2&entry.1019368948=20055&entry.452041058=Viktor"
	var error = http.request(url_submit, headers, HTTPClient.METHOD_GET, "")
	
	if error != OK:
		print("Error: %d" % error)
		http.queue_free()
	else:
		print("Request sent")

func Get_Id():
	http = HTTPRequest.new()
	http.request_completed.connect(http_done)
	add_child(http)
	
	var error = http.request(url_submit, headers, HTTPClient.METHOD_POST, "user_data")
	
	if error != OK:
		print("Error: %d" % error)
		http.queue_free()
	else:
		print("Request sent")
func UpdateMetadata():
	metadata_LevelTimePlayed = time
	if !first_request:
		metadata+=","
	else:
		first_request = false
	metadata += "\"level_" +str(metadata_Level)+  "\":{\"time_played\":"+str("%0.01f"%metadata_LevelTimePlayed)\
	+",\"attemps\":"+str(metadata_Attemps)  +  ",\"blocks_touched\":"+str(metadata_BlocksTouched)\
	+",\"remaining_lives\":"+str(metadata_RemainingLives) +",\"Undos\":"+str(metadata_Undo) + "}"
	RestartMetadata(true)
func RestartMetadata(Restart_All):
	metadata_Level = ""
	metadata_BlocksTouched = 0
	metadata_RemainingLives = 0
	
	if Restart_All:
		metadata_LevelTimePlayed = 0
		metadata_Attemps = 0
		metadata_Undo = 0
	
