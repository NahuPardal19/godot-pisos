extends Node2D

var rnd = RandomNumberGenerator.new()

##DATOS
@export_group("Datos")
@export var NumeroRondas : int
var NextLevelPath : String
var PreviousLevelPath : String
@export var ActualLevel : int
@export var Lifes : int
@onready var Inicio: Node2D = $"../Mapa/Inicio"
@onready var tilemap_level: TileMap = $"../Mapa/TilemapLevel"


@onready var Menu: Control = $"../UI/MenuPausa"
@onready var Transitioner: Transitioner = $"../UI/Transition"

##OBJECTS
@onready var deck: Control = $"../UI/Deck"

func _ready():
	if GAME_MANAGER.OnLevelEditorMode:
		IniciarLevel_Editor()
	else:
		IniciarLevel()
	
func IniciarLevel():
	#REQUERIMIENTOS GENERALES
	if ActualLevel > DATA_PLAYER.GameData.Level:
		DATA_PLAYER.GameData.Level = ActualLevel
		DATA_PLAYER.save_data()
	
	if ActualLevel < GAME_MANAGER.Max_Level:
		NextLevelPath = "res://Scenes/Levels/Level_" + str(ActualLevel+1) + ".tscn"
	else:
		NextLevelPath = "res://Scenes/Levels/menu.tscn"
	
	SHEET_MANAGER.metadata_Attemps += 1
	SHEET_MANAGER.metadata_Level = str(ActualLevel)
	
	GAME_MANAGER.CanUndo = false
	
	GAME_MANAGER.PosicionInicial = Inicio
	GAME_MANAGER.Map = tilemap_level
	
	GAME_MANAGER.RondasPasadas = 0
	
	GAME_MANAGER.PlayerList.clear()
	ChangeFloor.ChangeFloors.clear()
	GAME_MANAGER.FloorsToActivate.clear()
	GAME_MANAGER.ConvertFloors.clear()
	GAME_MANAGER.LastFloorTouched = null
	GAME_MANAGER.Transitioner = Transitioner
	Transitioner.visible = true
	UI_MANAGER.Menu = Menu
	
	#REQUERIMIENTOS SINGLEPLAYER
	GAME_MANAGER.SiguienteNivelPath = NextLevelPath
	GAME_MANAGER.AnteriorNivelPath = PreviousLevelPath
	
	GAME_MANAGER.RondasObjetivo = NumeroRondas
	UI_MANAGER.PutTextRound(str(NumeroRondas))		
	InitDeck()
		
	GAME_MANAGER.IniciarPartida()
	
	if Lifes <= 0:
		UI_MANAGER.Lifes.hide()
	else:
		GAME_MANAGER.PlayerList[0].Lifes = Lifes
		UI_MANAGER.PutLife(Lifes)
		GAME_MANAGER.LevelsLifes = Lifes
func IniciarLevel_Editor():
	GAME_MANAGER.CanUndo = false
	GAME_MANAGER.Map = tilemap_level
	
	while GAME_MANAGER.ReturnSpawn()== null:
		await get_tree().create_timer(0.1).timeout 
		
	GAME_MANAGER.PosicionInicial = GAME_MANAGER.ReturnSpawn().spawn
	GAME_MANAGER.RondasPasadas = 0
	
	GAME_MANAGER.PlayerList.clear()
	ChangeFloor.ChangeFloors.clear()
	GAME_MANAGER.FloorsToActivate.clear()
	GAME_MANAGER.ConvertFloors.clear()
	GAME_MANAGER.LastFloorTouched = null
	GAME_MANAGER.Transitioner = Transitioner
	Transitioner.visible = true
	UI_MANAGER.Menu = Menu
	
	#REQUERIMIENTOS SINGLEPLAYER
	if GAME_MANAGER.CantEdit:
		GAME_MANAGER.SiguienteNivelPath = "res://Scenes/Levels/menu.tscn"
		GAME_MANAGER.MenuOnEditor = true
	else:
		GAME_MANAGER.SiguienteNivelPath = "res://Scenes/Level Editor/Level_Editor.tscn"
	
	GAME_MANAGER.RestartOnStage = GAME_MANAGER.StageEnum.LevelEditorStage
	
	UI_MANAGER.PutTextRound(str(GAME_MANAGER.RondasObjetivo))		
	#InitDeck()
		
	GAME_MANAGER.IniciarPartida()
	
	GAME_MANAGER.PlayerList[0].Lifes = GAME_MANAGER.LevelsLifes
	
	if GAME_MANAGER.LevelsLifes <= 0:
		UI_MANAGER.Lifes.hide()
	else:
		UI_MANAGER.PutLife(GAME_MANAGER.LevelsLifes)

func InitDeck():
	GAME_MANAGER.Deck.clear()
	if deck.get_children().size() > 0:
		for card in deck.get_children():
			GAME_MANAGER.Deck.append(card)
	else:
		UI_MANAGER.Cards.hide()
