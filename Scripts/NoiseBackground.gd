extends Sprite2D

func _ready() -> void:
	UI_MANAGER.Background = $"."

func ChangeToGame():
	$AnimationPlayer.play("ChangeToGame")

func ChangeToPreparation():
	$AnimationPlayer.play("RESET")
