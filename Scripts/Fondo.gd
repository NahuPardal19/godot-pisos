extends Sprite2D

@onready var fondo = $"."
@onready var angle = 0
@export var anglePlus : float = 0.0005
@onready var animation_player: AnimationPlayer = $AnimationPlayer

func _ready():
	UI_MANAGER.Background = fondo
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Menu:
		ChangeToGame()

func _process(_delta):
	angle = angle + anglePlus
	fondo.material.set("shader_parameter/angle_radians",angle)

func ChangeToGame():
	animation_player.play("ChangeToGame")
	anglePlus = -0.0005

func ChangeToPreparation():
	animation_player.play("ChangeToPreparation")
	anglePlus = 0.0005
