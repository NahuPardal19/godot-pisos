extends Node2D

class_name FireBase_Manager

const FIRESTORE_URL := "https://firestore.googleapis.com/v1/projects/steps-levels/databases/(default)/documents/STEPS"
@onready  var http_request: HTTPRequest = $HTTPRequest

signal DataCompleted
var data

func _ready() -> void:
	#ENVIAR DATA
	var data = {
		"fields": {
			"nombre": {"stringValue": "viktor"},
			"mensaje": {"stringValue": "Hola desde Godot!"}
		}
	}
	#SendData(data)
	
	#RECIBIR DATA
	
	http_request.request_completed.connect(_on_request_completed)
	#GetData()

func SendData(data , nameP : String):
	# Convertimos el diccionario a una cadena JSON usando JSON.stringify()
	var json_data = JSON.stringify(data)

	# Encabezados para la solicitud HTTP
	var headers = ["Content-Type: application/json"]

	# Enviar la petición HTTP POST a Firestore
	# Nota: Usamos HTTPClient.METHOD_POST sin el true que estaba incorrectamente colocado.
	var error = http_request.request(FIRESTORE_URL+"/"+nameP, headers, HTTPClient.METHOD_PATCH, json_data)
	
	if error != OK:
		print("Error al enviar la solicitud: ", error)
func GetData():
		# Enviar una solicitud GET a Firestore
		var error = http_request.request(FIRESTORE_URL, [], HTTPClient.METHOD_GET)

		if error != OK:
			print("Error al enviar la solicitud: ", error)

func _on_request_completed(result: int, response_code: int, headers: Array, body: PackedByteArray):
	if response_code == 200:
		var body_string = body.get_string_from_utf8()

		# Crear una instancia de JSON
		var json_parser = JSON.new()
		var parse_result = json_parser.parse(body_string)

		# Verificar si el parseo fue exitoso
		if parse_result == OK:
			data = json_parser.get_data()  # Extraer los datos del resultado del parseo)
			DataCompleted.emit()
			## Si deseas iterar sobre los documentos obtenidos
			if data.has("documents"):
				print(data["documents"][1]["fields"]["Lifes"]["integerValue"])
				#for document in data["documents"]:
					# Crear un diccionario para cada documento
					#var dict = {}
					#var nombre = document.get("nombre", "SinNombre")  # Asigna un nombre o un valor por defecto
					#var contenido = document.get("fields", {})
					#print(document["fields"])
					#print(document["fields"]["mensaje"]["stringValue"]) 
					#print(document["fields"]["nombre"]["stringValue"]) 
					##print(contenido)
					#print("-------------")
		else:
			print("Error al parsear los datos: Código de error", parse_result.error)
	else:
		print("Error en la solicitud: Código", response_code)
