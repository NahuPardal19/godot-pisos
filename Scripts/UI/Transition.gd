extends Control

class_name Transitioner

#TRANSITION ROUND

@onready var texture_rect = $CircleTexture
@onready var animation_player = $AnimationPlayer

#TRANSITION LEVEL

@export var LevelName:String
@onready var color_rect = $ColorRect
@onready var label = $Label
@onready var audio_stream_player_2d = $AudioStreamPlayer2D

@onready var color_rect_up = $ColorRectUp
@onready var color_rect_down = $ColorRectDown

func _ready():
	color_rect_up.visible = false
	color_rect_down.visible = false
	texture_rect.visible = false 
	label.visible = false
	label.text = LevelName
func _CircleAnimation(Fading_Out : bool):
	if Fading_Out:
		animation_player.queue("Fade_Out Transition")
	else:
		animation_player.queue("Fade_In Transition")
func _LevelAnimation(Finish:bool):
	label.text = LevelName
	if Finish:
		animation_player.queue("Final_Nivel")
	else:
		animation_player.queue("Inicio_Nivel")
func _RestartAnimation(Init:bool):
	if Init:
		animation_player.play("Restart_Animation_Init")
	else:
		animation_player.play("Restart_Animation_Final")

##SOUNDS FUNCTIONS

func SonidoTyping():
	audio_stream_player_2d.stream = AUDIO_MANAGER.Typing
	audio_stream_player_2d.play()	
func SonidoFadeIn():
	audio_stream_player_2d.stream = AUDIO_MANAGER.FadeInSound
	audio_stream_player_2d.play()	
func SonidoFadeOut():
	audio_stream_player_2d.stream = AUDIO_MANAGER.FadeOutSound
	audio_stream_player_2d.play()
