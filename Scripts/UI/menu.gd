extends Control
@onready var audio_stream_player_2d = $AudioStreamPlayer2D

#@onready var floor_texture = $"LEVELS/HBoxContainer4/Floor Texture"
#@onready var jump_texture = $"LEVELS/HBoxContainer4/Jump Texture"
#@onready var number_texture = $"LEVELS/HBoxContainer4/Number Texture"
#@onready var incognit_texture = $"LEVELS/HBoxContainer4/? Texture"
var menu_before_options
var level_number = 0
const Max_Level : int = 18
@onready var fire_base_connector: FireBase_Manager = $"FireBase Connector"

var editor_levelButton = preload("res://Scenes/Level Editor/EditorLevelsButton.tscn")

func _ready():
	GAME_MANAGER.MapName = ""
	GAME_MANAGER.OnLevelEditorMode = false
	GAME_MANAGER.Stage = GAME_MANAGER.StageEnum.Menu
	DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_VISIBLE)
	GAME_MANAGER.RestartOnStage = GAME_MANAGER.StageEnum.Menu
	if GAME_MANAGER.MenuOnEditor:
		$"PLAY SELECTOR".show()
		$TITLE.hide()
		$SINGLEPLAYER.hide()
		$"Options Button".hide()
	
		#var task:FirestoreTask	= collection.update(data)
		_put_levels()
	else:
		$"SINGLEPLAYER/New Game".grab_focus()
	GAME_MANAGER.MenuOnEditor = false
	if OS.has_feature("web"):
		$"SINGLEPLAYER/Exit button".hide()
	if  DATA_PLAYER.GameData.Level <= 0:
		$"SINGLEPLAYER/Continue Button".disabled = true
		$"SINGLEPLAYER/Continue Button".texture_focused=load("res://Sprites/UI SPRITES/Button sprites/Button Disabled Selected.png")
		$"SINGLEPLAYER/Continue Button".texture_hover=load("res://Sprites/UI SPRITES/Button sprites/Button Disabled Selected.png")
	else:
		$"SINGLEPLAYER/Continue Button".disabled = false
		$"SINGLEPLAYER/Continue Button".texture_focused=load("res://Sprites/UI SPRITES/Button sprites/Button normal.png")
		$"SINGLEPLAYER/Continue Button".texture_focused=load("res://Sprites/UI SPRITES/Button sprites/Button Pressed.png")
		
	if GAME_MANAGER.RestartOnStage != GAME_MANAGER.StageEnum.LevelEditorStage:
		AUDIO_MANAGER.PlayMusic(AUDIO_MANAGER.MusicEnum.Menu,2,AUDIO_MANAGER.NormalVolume)
	LevelEditor.pathEditLevel = ""
	LevelEditor.LevelToEdit = false
	GAME_MANAGER.CantEdit = false
	GAME_MANAGER.Menu_Map_File = ""
func _on_options_button_focus_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_options_button_pressed():
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$SINGLEPLAYER.hide()
	$OPTIONS.show()
	$TITLE.hide()
	$"Options Button".hide()
	$OPTIONS/GENERAL/SliderVolume.grab_focus()

func _on_options_button_mouse_entered():
	$"Options Button".grab_focus()

#region SINGLEPLAYER



#FOCUS
func _on_play_button_focus_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_new_game_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_continue_button_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
	
	
#PRESSED

func _on_new_game_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	await audio_stream_player_2d.finished
	DATA_PLAYER.GameData.Level = 0
	DATA_PLAYER.save_data()
	get_tree().change_scene_to_file("res://Scenes/Levels/Level_0.tscn")
func _on_continue_button_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	var stringPath = "res://Scenes/Levels/Level_" + str(DATA_PLAYER.GameData.Level) + ".tscn"
	get_tree().change_scene_to_file(stringPath)

#MOUSE ENTERED

func _on_new_game_mouse_entered() -> void:
	$"SINGLEPLAYER/New Game".grab_focus()
func _on_continue_button_mouse_entered() -> void:
	if !$"SINGLEPLAYER/Continue Button".disabled:
		$"SINGLEPLAYER/Continue Button".grab_focus()
	
#endregion

#region OPTIONS
#FOCUS
func _on_options_back_button_focus_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

#PRESSED
func _on_options_back_button_pressed():
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$OPTIONS.hide()
	$SINGLEPLAYER.show()
	$TITLE.show()
	$"Options Button".show()
	$"SINGLEPLAYER/New Game".grab_focus()

#MOUSE ENTERED
func _on_options_back_button_mouse_entered():
	$"SINGLEPLAYER/New Game".grab_focus()

#endregion


func _on_level_back_button_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$TITLE.show()
	$SINGLEPLAYER.show()
	$"Options Button".show()
	$"LEVEL SELECTOR".hide()
	$"SINGLEPLAYER/New Game".grab_focus()
func _on_level_back_button_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_level_back_button_mouse_entered() -> void:
	$"LEVEL SELECTOR/Level Back Button".grab_focus()


func _on_levels_mouse_entered() -> void:
	$"LEVEL SELECTOR/Level Back Button".grab_focus()
func _on_levels_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_levels_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$"LEVEL SELECTOR".show()
	$SINGLEPLAYER.hide()
	$TITLE.hide()
	$"Options Button".hide()
	$"LEVEL SELECTOR/LEVELS/Level Button0".grab_focus()


func _on_exit_button_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	if !OS.has_feature("editor"):
		SHEET_MANAGER.Add()
		await SHEET_MANAGER.http.request_completed
		get_tree().quit()
	else:
		get_tree().quit()
func _on_exit_button_mouse_entered() -> void:
	$"SINGLEPLAYER/Exit button".grab_focus()
func _on_exit_button_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_level_editor_pressed() -> void:
	$TITLE.hide()
	$"Options Button".hide()
	$"LEVEL EDITOR".show()
	$SINGLEPLAYER.hide()
	$"LEVEL EDITOR/Play".grab_focus()
func _on_level_editor_mouse_entered() -> void:
	$"SINGLEPLAYER/Level Editor".grab_focus()
func _on_level_editor_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
	
#region LEVEL EDITOR BUTTONS
func _on_play_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$"LEVEL EDITOR".hide()
	$"PLAY SELECTOR".show()
func _on_play_mouse_entered() -> void:
	$"LEVEL EDITOR/Play".grab_focus()
func _on_play_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_create_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	get_tree().change_scene_to_file("res://Scenes/Level Editor/Level_Editor.tscn")
	
func _on_create_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_create_mouse_entered() -> void:
	$"LEVEL EDITOR/Create".grab_focus()

func _on_back_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$"LEVEL EDITOR".hide()
	$SINGLEPLAYER.show()
	$"Options Button".show()
	$TITLE.show()
	$"SINGLEPLAYER/New Game".grab_focus()
func _on_back_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_back_mouse_entered() -> void:
	$"LEVEL EDITOR/Back".grab_focus()

func _on_your_maps_pressed() -> void:
	$"LEVEL EDITOR LEVELS OFFLINE".show()
	$"LEVEL EDITOR".hide()
	$"PLAY SELECTOR".hide()
	GAME_MANAGER.IsOnline = false
	_put_levels()
func _on_your_maps_mouse_entered() -> void:
	$"PLAY SELECTOR/YOUR MAPS".grab_focus()
func _on_your_maps_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
	

func _on_other_maps_pressed() -> void:
	fire_base_connector.DataCompleted.connect(_put_online_levels)
	fire_base_connector.GetData()
	$"LEVEL EDITOR LEVELS ONLINE".show()
	GAME_MANAGER.IsOnline = true
	$"PLAY SELECTOR".hide()
func _on_other_maps_mouse_entered() -> void:
	$"PLAY SELECTOR/OTHER MAPS".grab_focus()
func _on_other_maps_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_back_to_level_editor_pressed() -> void:
	$"PLAY SELECTOR".hide()
	$"LEVEL EDITOR".show()
func _on_back_to_level_editor_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
func _on_back_to_level_editor_mouse_entered() -> void:
	$"PLAY SELECTOR/BackToLevelEditor".grab_focus()
#endregion

#region LEVEL EDITOR LEVELS BUTTONS

func _on_edit_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	if GAME_MANAGER.Menu_Map_File != null and GAME_MANAGER.Menu_Map_File != "":
		LevelEditor.LevelToEdit = true
		LevelEditor.pathEditLevel = GAME_MANAGER.Menu_Map_File
		get_tree().change_scene_to_file("res://Scenes/Level Editor/Level_Editor.tscn")
#func _on_edit_mouse_entered() -> void:
	#$"LEVEL EDITOR LEVELS/ActionLevelButtons/Edit".grab_focus()
func _on_edit_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_play_edit_level_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	GAME_MANAGER.CantEdit = true
	GAME_MANAGER.OnLevelEditorMode = true
	if GAME_MANAGER.Menu_Map_File != null and GAME_MANAGER.Menu_Map_File != "":
		GAME_MANAGER.LoadLevelEditorMap(GAME_MANAGER.Menu_Map_File)
#func _on_play_edit_level_mouse_entered() -> void:
	#$"LEVEL EDITOR LEVELS/ActionLevelButtons/PlayEditLevel".grab_focus()
func _on_play_edit_level_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_back_editor_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$"PLAY SELECTOR".show()
	$"LEVEL EDITOR LEVELS OFFLINE".hide()
func _on_back_editor_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
#func _on_back_editor_mouse_entered() -> void:
	#$"LEVEL EDITOR LEVELS/ActionLevelButtons/BackEditor".grab_focus()

#endregion
func _put_levels():
	for button in $"LEVEL EDITOR LEVELS OFFLINE/ScrollContainer/Levels".get_children():
		button.queue_free()
	var LevelNames = []
	LevelNames = get_map_names_from_clown_files()
	if LevelNames.size() > 0:
		for level_name in LevelNames:
			var editor_levelButton_instance = editor_levelButton.instantiate()
			editor_levelButton_instance.levelName = level_name
			$"LEVEL EDITOR LEVELS OFFLINE/ScrollContainer/Levels".add_child(editor_levelButton_instance)
func _put_online_levels():
	for button in $"LEVEL EDITOR LEVELS ONLINE/ScrollContainer/Levels".get_children():
		button.queue_free()
	var LevelNames = []
	#print(fire_base_connector.data["documents"][1]["fields"]["Name"]["stringValue"])
	for document in fire_base_connector.data["documents"]:
		LevelNames.append(document["fields"]["Name"]["stringValue"])

	if LevelNames.size() > 0:
		for level_name in LevelNames:
			var editor_levelButton_instance = editor_levelButton.instantiate()
			editor_levelButton_instance.levelName = level_name
			$"LEVEL EDITOR LEVELS ONLINE/ScrollContainer/Levels".add_child(editor_levelButton_instance)
	
func get_map_names_from_clown_files() -> Array:
	var map_names = []
	var dir = DirAccess.open("user://")
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if file_name.ends_with(".clown"):
				var map_name = file_name.substr(0, file_name.find(".clown"))
				if not map_name in map_names:
					map_names.append(map_name)
			file_name = dir.get_next()
		dir.list_dir_end()
	return map_names


func _on_online_play_edit_level_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	GAME_MANAGER.CantEdit = true
	GAME_MANAGER.OnLevelEditorMode = true
	
	if fire_base_connector.data != null and fire_base_connector.data["documents"].size() > 0:
		for document in fire_base_connector.data["documents"]:
			if document["fields"]["Name"]["stringValue"] == GAME_MANAGER.MapName:
				GAME_MANAGER.Online_Level = document
				break
				#GAME_MANAGER.Online_Level.append(document["fields"]["Name"]["stringValue"])
				#GAME_MANAGER.Online_Level.append(document["fields"]["Lifes"]["integerValue"])
				#GAME_MANAGER.Online_Level.append(document["fields"]["Rounds"]["integerValue"])
				#GAME_MANAGER.Online_Level.append(document["fields"]["Map"])
				#GAME_MANAGER.Online_Level.append(document["fields"]["Cards"])
		
				
		
	if GAME_MANAGER.MapName != null and GAME_MANAGER.MapName != "":
		get_tree().change_scene_to_file("res://Scenes/Level Editor/MapLoader.tscn")

func _on_online_play_edit_level_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_online_back_editor_pressed() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	$"LEVEL EDITOR LEVELS ONLINE".hide()
	$"PLAY SELECTOR".show()
func _on_online_back_editor_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()


func _on_delete_pressed() -> void:
	pass # Replace with function body.
func _on_delete_mouse_entered() -> void:
	pass # Replace with function body.
func _on_delete_focus_entered() -> void:
	pass # Replace with function body.
