extends Node

#region UI SCORE , INPUT & LIFE

#GENERAL

@export var Tutorial : bool

@onready var amounts: Control = $Amounts

@onready var rounds: HBoxContainer = $Amounts/Rondas
@onready var lifes: HBoxContainer = $Amounts/Lifes
@onready var undo: HBoxContainer =$Amounts/Undo

@onready var edit: HBoxContainer = $EDIT


##RESTART
#@onready var r_keyboard: Label = $Buttons/Restart/R_KEYBOARD
#@onready var b_xbox: TextureRect = $Buttons/Restart/B_XBOX
#@onready var separation: Label = $Buttons/Restart/Separation
#@onready var circle_play_station: TextureRect = $Buttons/Restart/Circle_PlayStation
#
#
###JUMP
#@onready var space_keyboard: Label = $Buttons/Jump/SPACE_KEYBOARD
#@onready var separation_keyboard_2: Label = $Buttons/Jump/SEPARATION_KEYBOARD2
#@onready var w_keyboard: Label = $Buttons/Jump/W_KEYBOARD
#
#@onready var a_xbox: TextureRect = $Buttons/Jump/A_XBOX
#@onready var separation2 : Label = $Buttons/Jump/Separation
#@onready var x_play_station: TextureRect = $Buttons/Jump/X_PlayStation

#endregion

#region UI CARDS
@onready var cards_container= $PanelCards

#endregion
func _ready():
	_InitUI()

func _process(_delta):
	_ControlInput()
	if GAME_MANAGER.Stage == GAME_MANAGER.StageEnum.Game and GAME_MANAGER.LevelEdited:
		edit.show()
	else:
		edit.hide()
	
func _InitUI():
	UI_MANAGER.TextoRondas = rounds
	UI_MANAGER.Lifes = lifes
	UI_MANAGER.Cards = cards_container
	UI_MANAGER.Undo = undo
	#if !Tutorial:
		#amounts.hide()
func _ControlInput():
	if Input.is_action_pressed("ControllerInput"):
		GAME_MANAGER.OnController = true
	elif Input.is_action_pressed("KeyboardInput"):
		GAME_MANAGER.OnController = false
	
	
	if GAME_MANAGER.OnController:
		#RESTART
		$Buttons/Restart/R_input.hide()
		$Buttons/Restart/Y_input.show()
		$Buttons/Restart/Triangle_input.show()
		
		#JUMP
		$Buttons/Jump/X_input.show()
		$Buttons/Jump/A_input.show()
		$Buttons/Jump/W_input.hide()
		
		#EDIT
		$EDIT/Circle_Playstation.show()
		$EDIT/E_input.hide()
		$EDIT/B_Xbox.show()
	else:
		#RESTART
		$Buttons/Restart/R_input.show()
		$Buttons/Restart/Y_input.hide()
		$Buttons/Restart/Triangle_input.hide()
		
		#JUMP
		$Buttons/Jump/X_input.hide()
		$Buttons/Jump/A_input.hide()
		$Buttons/Jump/W_input.show()
		
		#EDIT
		$EDIT/Circle_Playstation.hide()
		$EDIT/E_input.show()
		$EDIT/B_Xbox.hide()
