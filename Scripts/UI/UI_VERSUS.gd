extends Control

class_name UI_VERSUS
@onready var playersfloor = $Playersfloor

@onready var player_1: HBoxContainer = $Playersfloor/Player1
@onready var player_2: HBoxContainer = $Playersfloor/Player2
@onready var player_3: HBoxContainer = $Playersfloor/Player3
@onready var player_4: HBoxContainer = $Playersfloor/Player4

@onready var easy_consequence: HBoxContainer = $"VBoxContainer/Easy Consequence"
@onready var medium_consequence: HBoxContainer = $"VBoxContainer/Medium Consequence"
@onready var hard_consequence: HBoxContainer = $"VBoxContainer/Hard Consequence"
@onready var death: HBoxContainer = $VBoxContainer/Death

@onready var panel_time = $Label


func _ready():
	UI_MANAGER.Versus = get_node(".")

func _process(delta):
	pass
