extends TextureButton
@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer
@export var Level : int


func _ready() -> void:
	if DATA_PLAYER.GameData.Level < Level and false:
		disabled = true
		texture_focused=load("res://Sprites/UI SPRITES/Button sprites/Level Button disabled selected.png")
		texture_hover=load("res://Sprites/UI SPRITES/Button sprites/Level Button disabled selected.png")
	else:
		disabled = false
		texture_focused=load("res://Sprites/UI SPRITES/Button sprites/Level Button TOUCHED.png")
		texture_hover=load("res://Sprites/UI SPRITES/Button sprites/Level Button TOUCHED.png")

func _on_pressed() -> void:
	audio_stream_player.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player.play()
	await audio_stream_player.finished
	get_tree().change_scene_to_file("res://Scenes/Levels/Level_" + str(Level) + ".tscn")
func _on_mouse_entered() -> void:
	grab_focus()
func _on_focus_entered() -> void:
	if !disabled:
		audio_stream_player.stream = AUDIO_MANAGER.SelectedButton
		audio_stream_player.play()
