extends HSlider

@onready var audio_stream_player_2d = $AudioStreamPlayer2D


@export var Bus_Name : String
@export var button : TextureButton
var Bus_Index : int

@export var buttonNoFocused : Texture
@export var buttonFocused : Texture

func _ready():
	Bus_Index = AudioServer.get_bus_index(Bus_Name)
	value = db_to_linear(AudioServer.get_bus_volume_db(Bus_Index))

func _on_value_changed(value):
	AudioServer.set_bus_volume_db(Bus_Index,linear_to_db(value))

func _on_focus_entered():
	button.texture_normal = buttonFocused
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_focus_exited():
	button.texture_normal = buttonNoFocused

func _on_mouse_entered():
	$".".grab_focus()


func _on_drag_ended(value_changed: bool) -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
