extends Control

@onready var Menu = $"."
@onready var audio_stream_player_2d = $AudioStreamPlayer2D
@onready var NormalSide = $NORMAL
@onready var options = $OPTIONS
@onready var back_button = $"OPTIONS/Back Button"
@onready var resume_button = $"NORMAL/Resume Button"
@onready var slider_volume = $OPTIONS/GENERAL/SliderVolume

func _ready() -> void:
	if GAME_MANAGER.OnLevelEditorMode and !GAME_MANAGER.CantEdit:
		$NORMAL/Back_To_Editor.show()
		$"NORMAL/Back_To_Menu Button".hide()
	else:
		$NORMAL/Back_To_Editor.hide()
		$"NORMAL/Back_To_Menu Button".show()

func _physics_process(_delta):
	if Input.is_action_just_pressed("Singleplayer_Pause"):
		audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
		audio_stream_player_2d.play()

func _on_resume_button_pressed():
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	GAME_MANAGER.Pause()

func _on_options_button_pressed():
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	slider_volume.grab_focus()
	options.show()
	back_button.show()
	NormalSide.hide()

func _on_back_to_menu_button_pressed():
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	GAME_MANAGER.Deck.clear()
	GAME_MANAGER.FinPartida.emit()
	await audio_stream_player_2d.finished
	GAME_MANAGER.GoToMenu()

func _on_resume_button_mouse_entered():
	$"NORMAL/Resume Button".grab_focus()

func _on_options_button_mouse_entered():
	$"NORMAL/Options Button".grab_focus()
	


func _on_back_to_menu_button_mouse_entered():
	$"NORMAL/Back_To_Menu Button".grab_focus()


func _on_slider_volume_drag_ended(_value_changed):
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()


func _on_back_button_pressed():
	audio_stream_player_2d.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player_2d.play()
	await audio_stream_player_2d.finished
	resume_button.grab_focus()
	options.hide()
	back_button.hide()
	NormalSide.show()

func _on_back_button_mouse_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()


func _on_draw():
	resume_button.grab_focus()
	options.hide()
	back_button.hide()
	NormalSide.show()


func _on_resume_button_focus_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()


func _on_options_button_focus_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()

func _on_back_to_menu_button_focus_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()


func _on_back_button_focus_entered():
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()


func _on_back_to_editor_pressed() -> void:
	LevelEditor.LevelToEdit = true
	GAME_MANAGER.MenuOnEditor = true
	get_tree().change_scene_to_file("res://Scenes/Level Editor/Level_Editor.tscn")
func _on_back_to_editor_mouse_entered() -> void:
	$NORMAL/Back_To_Editor.grab_focus()
func _on_back_to_editor_focus_entered() -> void:
	audio_stream_player_2d.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player_2d.play()
