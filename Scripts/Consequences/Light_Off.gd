extends Consequence

@onready var directional_light_2d = $DirectionalLight2D

func PutConsequenceOnPlayer(player:Player):
	directional_light_2d.show()
	player.point_light_2d.show()

func RemoveConsequence(player:Player):
	directional_light_2d.hide()
	player.point_light_2d.hide()
