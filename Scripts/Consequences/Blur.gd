extends Consequence

@onready var blur: Sprite2D = $Blur


func PutConsequenceOnPlayer(player:Player):
	blur.show()

func RemoveConsequence(player:Player):
	blur.hide()
