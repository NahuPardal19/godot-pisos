extends Coin

@export var HideCards : bool 

func OnContact(body):
	if HideCards:
		for card in GAME_MANAGER.Deck:
			card.Hide()
	else:
		for card in GAME_MANAGER.Deck:
			card.ShowCard()
		

func _on_body_entered(body: Node2D) -> void:
	OnContact(body)
	queue_free()
