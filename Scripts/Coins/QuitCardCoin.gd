extends Coin

func OnContact(body):
	GAME_MANAGER.QuitCard(GAME_MANAGER.GetFirstCard())

func _on_body_entered(body: Node2D) -> void:
	OnContact(body)
	queue_free()
