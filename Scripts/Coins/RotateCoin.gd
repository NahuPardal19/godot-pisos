extends Coin

func OnContact(body):
	GAME_MANAGER.Deck.reverse()
	for card in GAME_MANAGER.Deck:
			UI_MANAGER.PutCard(card)

func _on_body_entered(body: Node2D) -> void:
	OnContact(body)
	queue_free()
