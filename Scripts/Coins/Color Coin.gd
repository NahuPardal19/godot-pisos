extends Coin

@export var ColorCard : Floor.ColorEnum

func OnContact(body):
	GoToCard()

func GoToCard():
	for card in GAME_MANAGER.Deck:
		if card.ColorFloor == ColorCard:
			GAME_MANAGER.Deck.erase(card)
			GAME_MANAGER.Deck.insert(0,card)
			for cardDeck in GAME_MANAGER.Deck:
				UI_MANAGER.PutCard(cardDeck)
			return
			
func _on_body_entered(body: Node2D) -> void:
	OnContact(body)
	queue_free()
