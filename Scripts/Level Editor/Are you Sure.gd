extends PanelContainer

var IsBlock

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func Active(IsBlockP : bool):
	show()
	IsBlock = IsBlockP

func _on_yes_pressed() -> void:
	if IsBlock:
		$"../.."._Remove_All_Instances()
	else:
		$"../..".ClearCards()
	hide()
func _on_no_pressed() -> void:
	hide()
