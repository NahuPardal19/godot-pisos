extends PanelContainer

var filePath : String 
var leveltosave
var IsTest : bool
@onready var fire_base_connector: FireBase_Manager = $"../../FireBase Connector"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func Active(filepath : String , LevelToSave,IsTestP):
	show()
	filePath = filepath
	leveltosave = LevelToSave
	IsTest = IsTestP


func _on_yes_override_pressed() -> void:
	print(leveltosave[0])
	if $"../..".public:
		var data = {
		"fields": {
			"Name": {"stringValue": leveltosave[0]["Name"]},
			"Rounds": {"integerValue":  leveltosave[0]["Rounds"]},
			"Lifes": {"integerValue":  leveltosave[0]["Lifes"]},
			"Map": convertir_array_a_firestore_MAP( leveltosave[0]["Map"]),
			"Cards": convertir_array_a_firestore_CARDS( leveltosave[0]["Cards"]),
			#"CardsButtons": {"stringValue": DeckButtons},
		}
		}
		fire_base_connector.SendData(data, leveltosave[0]["Name"])
	var file = FileAccess.open(filePath, FileAccess.WRITE)
	file.store_string(JSON.stringify(leveltosave))
	file = null
	hide()
	if IsTest:
		LevelEditor.pathEditLevel = filePath
		await fire_base_connector.http_request.request_completed
		GAME_MANAGER.LoadLevelEditorMap(filePath)

func convertir_array_a_firestore_MAP(array: Array) -> Dictionary:
	var array_result = {"arrayValue": {"values": []}}
	for item in array:
		var firestore_item = {
		"mapValue": {
		"fields": {
		"filePath": {"stringValue": item["filePath"]},
		"position": {
		"mapValue": {
		"fields": {
		"x": {"integerValue": int(item["position"].x)},
		"y": {"integerValue": int(item["position"].y)}
		}
		}
		}
		}
		}
		}
		array_result["arrayValue"]["values"].append(firestore_item)
	return array_result
func convertir_array_a_firestore_CARDS(array: Array) -> Dictionary:
	var array_result = {"arrayValue": {"values": []}}
	for item in array:
		var firestore_item = {
		"mapValue": {
		"fields": {
		"filePath": {"stringValue": item["filePath"]},
		"color": {"stringValue": item["color"]}
		}
		}
		}
		array_result["arrayValue"]["values"].append(firestore_item)
	return array_result

func _on_no_override_pressed() -> void:
	hide()
	leveltosave = null
	filePath = ""
