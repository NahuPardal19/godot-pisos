extends Node2D

class_name LevelEditor
# Variables para almacenar la escena que quieres instanciar y el TileMap
@onready var tilemap_level: TileMap = $TilemapLevel
@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer

static var ActualFloorIsConnector : bool
static var ActualFloorCanConnect : bool
static var ActualFloorIsPreparation : bool
static var FloorPath : String

static var DeckcardsContainer: GridContainer
static var DeckButtons = []
var CardButton = preload("res://Scenes/Level Editor/Deck Card Button.tscn")

static var CanBuild : bool
static var OnMenu : bool

@onready var are_you_sure: PanelContainer = $"UI/Are you Sure"

@onready var blocks_container: HBoxContainer = $"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container"
@onready var cards_container: HBoxContainer = $"UI/VBoxContainer/PanelContainer/MarginContainer/Cards Container"

var Clown_Mode : bool = true
static var instances = []

static var LevelToEdit : bool
static var pathEditLevel : String

var public : bool = true
@onready var fire_base_connector: FireBase_Manager = $"FireBase Connector"

#region LEVEL VARIABLES
var Cards = []
var Name : String
var Rounds : int
var Lifes : int
#endregion

func _ready() -> void:
	if GAME_MANAGER.RestartOnStage != GAME_MANAGER.StageEnum.Menu:
		AUDIO_MANAGER.PlayMusic(AUDIO_MANAGER.MusicEnum.Menu,2,AUDIO_MANAGER.NormalVolume)
	GAME_MANAGER.Stage = GAME_MANAGER.StageEnum.LevelEditorStage
	GAME_MANAGER.OnLevelEditorMode = true
	FloorPath = "res://Scenes/Floors/Normal floor/WhiteFloor.tscn"
	ActualFloorIsConnector = false
	ActualFloorIsPreparation = false
	CanBuild = false
	OnMenu = false
	Cards.clear()
	instances.clear()
	DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_VISIBLE)
	DeckButtons.clear()
	GAME_MANAGER.Level_Loaded = false
	GAME_MANAGER.MenuOnEditor = false
	DeckcardsContainer = $UI/VBoxContainer/MarginContainer/HBoxContainer/PanelContainer/GridContainer
	if LevelToEdit:
		Load_LevelEditorMap(pathEditLevel)
		OnMenu = false
		_Check_Spawner()

func _process(delta: float) -> void:
	if CanBuild:
		_Level_input()

# Función para instanciar una escena en una celda específica
func _instance_scene_at_cell(cell_x: int, cell_y: int):
	for i in range(instances.size()):
		if instances[i]["cell"] == Vector2(cell_x, cell_y):
			if !ActualFloorIsConnector:
				_remove_instance_at_cell(cell_x,cell_y)
				var floorToLoad = load(FloorPath)
				var floorToInstance = floorToLoad.instantiate()
				var cell_position = tilemap_level.map_to_local(Vector2(cell_x, cell_y))
				
				floorToInstance.position = cell_position + Vector2(tilemap_level.tile_set.tile_size.x , tilemap_level.tile_set.tile_size.y) / 2
				tilemap_level.add_child(floorToInstance)
				
				instances.append({"instance": floorToInstance, "cell": Vector2(cell_x, cell_y), "IsConnector": ActualFloorIsConnector , "CanConnect": ActualFloorCanConnect})
				return
			else:
				if instances[i]["CanConnect"]:
					if instances[i]["IsConnector"]: 
						_remove_instance_at_cell(cell_x,cell_y)
					var floorToLoad = load(FloorPath)
					var floorToInstance = floorToLoad.instantiate()
					var cell_position = tilemap_level.map_to_local(Vector2(cell_x, cell_y))
					
					floorToInstance.position = cell_position + Vector2(tilemap_level.tile_set.tile_size.x , tilemap_level.tile_set.tile_size.y) / 2
					tilemap_level.add_child(floorToInstance)
					
					instances.append({"instance": floorToInstance, "cell": Vector2(cell_x, cell_y), "IsConnector": ActualFloorIsConnector , "CanConnect": ActualFloorCanConnect })
					return
	
	if !ActualFloorIsConnector:
		var floorToLoad = load(FloorPath)
		var floorToInstance = floorToLoad.instantiate()
		var cell_position = tilemap_level.map_to_local(Vector2(cell_x, cell_y))
					
		floorToInstance.position = cell_position + Vector2(tilemap_level.tile_set.tile_size.x , tilemap_level.tile_set.tile_size.y) / 2
		tilemap_level.add_child(floorToInstance)
					
		instances.append({"instance": floorToInstance, "cell": Vector2(cell_x, cell_y), "IsConnector": ActualFloorIsConnector , "CanConnect": ActualFloorCanConnect})
func _remove_instance_at_cell(cell_x: int, cell_y: int):
	for i in range(instances.size() - 1, -1, -1):
		if instances[i]["cell"] == Vector2(cell_x, cell_y):
			instances[i]["instance"].queue_free()
			instances.remove_at(i)
			break
func _Remove_All_Instances():
	for i in range(instances.size() - 1, -1, -1):
		instances[i]["instance"].queue_free()
		instances.remove_at(i)
	_Check_Spawner()

func ClearCards():
	for button_card in DeckcardsContainer.get_children():
		button_card.queue_free()

func _Level_input():
	
	if Input.is_action_just_pressed("LeftClick"):
		var mouse_pos = tilemap_level.get_global_mouse_position()
		var local_pos = tilemap_level.to_local(mouse_pos)
		var cell = tilemap_level.local_to_map(local_pos)
		var cell_origin = tilemap_level.map_to_local(cell)
		
		if local_pos.x < cell_origin.x:
			cell.x -= 1
		if local_pos.y < cell_origin.y:
			cell.y -= 1
		if (!ActualFloorIsPreparation or (ActualFloorIsPreparation and DeckcardsContainer.get_children().size() > 0)) and FloorPath != "":
			_instance_scene_at_cell(cell.x, cell.y)
			_Check_Spawner()

	if Input.is_action_just_pressed("RightClick"):
		var mouse_pos = tilemap_level.get_global_mouse_position()
		var local_pos = tilemap_level.to_local(mouse_pos)
		var cell = tilemap_level.local_to_map(local_pos)
		var cell_origin = tilemap_level.map_to_local(cell)

		if local_pos.x < cell_origin.x:
			cell.x -= 1
		if local_pos.y < cell_origin.y:
			cell.y -= 1

		_remove_instance_at_cell(cell.x, cell.y)
		_Check_Spawner()
func _Check_Spawner():
	for i in range(instances.size() - 1, -1, -1):
		if instances[i]["instance"].scene_file_path == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn":
			$"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container/Blocks Buttons/Active Spawner".disabled = true
			$"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container/Blocks Buttons/Active Spawner".texture_focused = $"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container/Blocks Buttons/Active Spawner".texture_disabled
			if FloorPath == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn":
				FloorPath = ""
			return
	$"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container/Blocks Buttons/Active Spawner".disabled = false
	$"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container/Blocks Buttons/Active Spawner".texture_focused = $"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container/Blocks Buttons/Active Spawner".texture_normal

static func DestroyBlock(block):
	for i in range(instances.size() - 1, -1, -1):
		if instances[i]["instance"] == block:
			instances.remove_at(i)
			block.queue_free()

func Load_LevelEditorMap(path:String):
	var file = FileAccess.open(path,FileAccess.READ)
	var LevelToLoad = JSON.parse_string(file.get_as_text())
	DeckButtons.clear()
	#Name
	Name = LevelToLoad[0]["Name"]
	$UI/Settings/VBoxContainer/Name/HBoxContainer2/NameButton.text = Name
	
	#ROUNDS
	Rounds = LevelToLoad[0]["Rounds"]
	$UI/Settings/VBoxContainer/Rounds/HBoxContainer2/RoundButton.text = str(Rounds)
	
	#Lifes
	Lifes = LevelToLoad[0]["Lifes"]
	$UI/Settings/VBoxContainer/Lifes/HBoxContainer2/LifeButton.text = str(Lifes)
	
	#Map
	var LevelP = []
	LevelP = LevelToLoad[0]["Map"]
	for block in LevelP:
		var block_path = block["filePath"]
		var block_position = block["position"]
		var blockLoaded = load(block_path)
		if blockLoaded != null:
			var block_instance = blockLoaded.instantiate()
			block_instance.position = GAME_MANAGER._string_to_vector2(block_position)
			tilemap_level.add_child(block_instance)
			
			var local_pos = tilemap_level.to_local(block_instance.position)
			var cell = tilemap_level.local_to_map(block_instance.position)
			
			cell -= Vector2i.ONE
			
			var IsConnector : bool = false
			var CanConnect : bool = true
			if block_instance is Connector:
				IsConnector = true
			if block_path == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn" or block_path == "res://Scenes/Floors/Game Floors/SpawnerDesactivated.tscn" or block_instance is PreparationFloor:
				CanConnect = false
				
			instances.append({"instance": block_instance, "cell": Vector2(cell.x, cell.y), "IsConnector": IsConnector , "CanConnect": CanConnect})
	
	#CARDS
	Cards = LevelToLoad[0]["Cards"]
	DeckButtons = LevelToLoad[0]["CardsButtons"]
	if 	DeckButtons.size()<=0:
		Clown_Mode = true
		_on_button_act_pressed()
	else:
		for cardButton in DeckButtons:
			var cardInstance : CardButton_LevelEditor = CardButton.instantiate()
			cardInstance.color = cardButton["Color"]
			cardInstance.FilePath = cardButton["FilePath"]
			cardInstance.texture_normal = load(cardButton["Texture"])
			cardInstance.texture_disabled = load(cardButton["Texture"])
			cardInstance.texture_focused = load(cardButton["Texture"])
			cardInstance.texture_hover = load(cardButton["Texture"])
			cardInstance.texture_pressed = load(cardButton["Texture"])
			LevelEditor.DeckcardsContainer.add_child(cardInstance)
	
	#Online state
	public = !LevelToLoad[0]["Public"]
	_on_public_button_act_pressed()
	public = LevelToLoad[0]["Public"]
	
	
func SaveMap():
		#Name
	var stripped_string = $UI/Settings/VBoxContainer/Name/HBoxContainer2/NameButton.text.strip_edges()

	if stripped_string.length() > 0:
		Name = $UI/Settings/VBoxContainer/Name/HBoxContainer2/NameButton.text
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "The Name is Empty"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	#Rounds
	if _AreOnlyNumbers($UI/Settings/VBoxContainer/Rounds/HBoxContainer2/RoundButton.text) and int($UI/Settings/VBoxContainer/Rounds/HBoxContainer2/RoundButton.text)>0:
		Rounds = int($UI/Settings/VBoxContainer/Rounds/HBoxContainer2/RoundButton.text)
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "Only positive numbers in Rounds"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	#Lifes
	if _AreOnlyNumbers($UI/Settings/VBoxContainer/Lifes/HBoxContainer2/LifeButton.text) and int($UI/Settings/VBoxContainer/Lifes/HBoxContainer2/LifeButton.text)>0:
		Lifes = int($UI/Settings/VBoxContainer/Lifes/HBoxContainer2/LifeButton.text)
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "Only positive numbers in Lifes"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	var contadorSpawn = 0
	var contadorWinsBlock = 0
	for i in range(instances.size() - 1, -1, -1):
		if instances[i]["instance"].scene_file_path == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn":
			contadorSpawn+=1
		elif instances[i]["instance"].scene_file_path == "res://Scenes/Floors/Game Floors/Win Floors/IndestructibleWinFloor.tscn" or instances[i]["instance"].scene_file_path == "res://Scenes/Floors/Game Floors/Win Floors/WinFloor.tscn":
			contadorWinsBlock+=1
	if contadorSpawn <= 0:
		$UI/SaveError/VBoxContainer/TypeError.text = "There is no Spawn Block"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	#if contadorWinsBlock <= 0:
		#$UI/SaveError/VBoxContainer/TypeError.text = "There is no Win Block"
		#$UI/SaveError.show()
		#$UI/Settings.hide()
		#return
	
	#Map
	var map = []
	if tilemap_level.get_children().size() > 0:
		for block in tilemap_level.get_children():
			map.append({
				"filePath" = block.scene_file_path,
				"position" = block.position
			})
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "There are no Blocks"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	#Cards
	Cards.clear()
	if $UI/VBoxContainer/MarginContainer/HBoxContainer/PanelContainer/GridContainer.get_children().size() > 0 or !Clown_Mode:
		for card in $UI/VBoxContainer/MarginContainer/HBoxContainer/PanelContainer/GridContainer.get_children():
			Cards.append({
				"filePath" = card.FilePath,
				"color" = card.color
			})
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "There are no Cards"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	DeckButtons.clear()
	for cardButton in DeckcardsContainer.get_children():
		DeckButtons.append({
			"FilePath" = cardButton.FilePath,
			"Color" = cardButton.color,
			"Texture" = cardButton.texture_normal.resource_path
		})
	var LevelToSave = []
	
	LevelToSave.append({
		"Name" = Name,
		"Map" = map,
		"Cards" = Cards,
		"CardsButtons" = DeckButtons,
		"Rounds" = Rounds,
		"Lifes" = Lifes,
		"Public" = public
	})
	

	
	if _file_exists_in_user_data("user://" + Name + ".clown"):
		$UI/OverWrite.Active("user://" + Name + ".clown",LevelToSave,false)
		return
	
	if public:
		var data = {
		"fields": {
			"Name": {"stringValue": Name},
			"Rounds": {"integerValue": Rounds},
			"Lifes": {"integerValue": Lifes},
			"Map": convertir_array_a_firestore_MAP(map),
			"Cards": convertir_array_a_firestore_CARDS(Cards),
			#"CardsButtons": {"stringValue": DeckButtons},
		}
		}
		fire_base_connector.SendData(data, Name)	
	
	##SAVE
	var file = FileAccess.open("user://" + Name + ".clown" , FileAccess.WRITE)
	file.store_string(JSON.stringify(LevelToSave))
	file = null
	
func _AreOnlyNumbers(texto: String) -> bool:
	var regex = RegEx.new()
	regex.compile("^[0-9]+$")

	return regex.search(texto) != null
func _file_exists_in_user_data(file_name: String) -> bool:
	var file = FileAccess.open(file_name, FileAccess.READ)
	if file:
		file.close()
		return true
	return false

#region BUTTON EVENTS
func _on_cards_pressed() -> void:
	blocks_container.hide()
	cards_container.show()
func _on_blocks_pressed() -> void:
	blocks_container.show()
	cards_container.hide()

#region BLOCK BUTTONS
func _on_clear_pressed() -> void:
	audio_stream_player.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player.play()
	are_you_sure.Active(true)
#endregion
#region CARDS BUTTONS
func _on_card_clear_pressed() -> void:
	audio_stream_player.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player.play()
	are_you_sure.Active(false)

func _on_button_white_pressed() -> void:
	for button_card in $"UI/VBoxContainer/PanelContainer/MarginContainer/Cards Container/Cards Button Container".get_children():
		button_card.ChangeToColor(Card_Button.ColorCard_Enum.White)
func _on_button_yellow_pressed() -> void:
	for button_card in $"UI/VBoxContainer/PanelContainer/MarginContainer/Cards Container/Cards Button Container".get_children():
		button_card.ChangeToColor(Card_Button.ColorCard_Enum.Yellow)
func _on_button_green_pressed() -> void:
	for button_card in $"UI/VBoxContainer/PanelContainer/MarginContainer/Cards Container/Cards Button Container".get_children():
		button_card.ChangeToColor(Card_Button.ColorCard_Enum.Green)
#endregion


#region SETTINGS BUTTONS
func _on_button_act_pressed() -> void:
	if Clown_Mode:
		Clown_Mode = false
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.disabled = true
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_normal = load("res://Sprites/Level Editor Sprites/Button Desactivated x300.png")
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_hover = load("res://Sprites/Level Editor Sprites/Button Desactivated x300.png")
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_pressed = load("res://Sprites/Level Editor Sprites/Button Desactivated x300.png")
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_focused = load("res://Sprites/Level Editor Sprites/Button Desactivated x300.png")
		
		$"UI/VBoxContainer/PanelContainer/MarginContainer/Blocks Container".show()
		$"UI/VBoxContainer/PanelContainer/MarginContainer/Cards Container".hide()
		$UI/VBoxContainer/MarginContainer/HBoxContainer/PanelContainer.hide()
		
		ClearCards()
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Clown Mode/ButtonAct".texture_focused = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Red.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Clown Mode/ButtonAct".texture_hover = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Red Pressed.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Clown Mode/ButtonAct".texture_normal = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Red.png")
	else:
		
		Clown_Mode = true
		
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_normal = load("res://Sprites/Level Editor Sprites/Button Normal x300.png")
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_hover = load("res://Sprites/Level Editor Sprites/Button Pressed x300.png")
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_pressed = load("res://Sprites/Level Editor Sprites/Button Pressed x300.png")
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.texture_focused = load("res://Sprites/Level Editor Sprites/Button Pressed x300.png")
		$UI/VBoxContainer/MarginContainer/HBoxContainer/Cards.disabled = false
		$UI/VBoxContainer/MarginContainer/HBoxContainer/PanelContainer.show()
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Clown Mode/ButtonAct".texture_focused = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Green.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Clown Mode/ButtonAct".texture_hover = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Green Pressed.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Clown Mode/ButtonAct".texture_normal = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Green.png")

func _on_public_button_act_pressed() -> void:
	if public:
		public = false
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Public/PublicButtonAct".texture_focused = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Red.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Public/PublicButtonAct".texture_hover = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Red Pressed.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Public/PublicButtonAct".texture_normal = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Red.png")
	else:
		public = true
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Public/PublicButtonAct".texture_focused = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Green.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Public/PublicButtonAct".texture_hover = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Green Pressed.png")
		$"UI/Settings/VBoxContainer/Clown Mode and Public options/Public/PublicButtonAct".texture_normal = load("res://Sprites/Level Editor Sprites/Cards/Buttons/Button Green.png")
	
func _on_save_pressed() -> void:
	SaveMap()
func _on_ok_pressed() -> void:
	$UI/Settings.show()
	$UI/SaveError.hide()
func _on_test_map_pressed() -> void:
		#Name
	var stripped_string = $UI/Settings/VBoxContainer/Name/HBoxContainer2/NameButton.text.strip_edges()

	if stripped_string.length() > 0:
		Name = $UI/Settings/VBoxContainer/Name/HBoxContainer2/NameButton.text
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "The Name is Empty"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	#Rounds
	if _AreOnlyNumbers($UI/Settings/VBoxContainer/Rounds/HBoxContainer2/RoundButton.text) and int($UI/Settings/VBoxContainer/Rounds/HBoxContainer2/RoundButton.text)>0:
		Rounds = int($UI/Settings/VBoxContainer/Rounds/HBoxContainer2/RoundButton.text)
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "Only positive numbers in Rounds"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	#Lifes
	if _AreOnlyNumbers($UI/Settings/VBoxContainer/Lifes/HBoxContainer2/LifeButton.text) and int($UI/Settings/VBoxContainer/Lifes/HBoxContainer2/LifeButton.text)>0:
		Lifes = int($UI/Settings/VBoxContainer/Lifes/HBoxContainer2/LifeButton.text)
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "Only positive numbers in Lifes"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	var contadorSpawn = 0
	var contadorWinsBlock = 0
	for i in range(instances.size() - 1, -1, -1):
		if instances[i]["instance"].scene_file_path == "res://Scenes/Floors/Game Floors/SpawnerActivated.tscn":
			contadorSpawn+=1
		elif instances[i]["instance"].scene_file_path == "res://Scenes/Floors/Game Floors/Win Floors/IndestructibleWinFloor.tscn" or instances[i]["instance"].scene_file_path == "res://Scenes/Floors/Game Floors/Win Floors/WinFloor.tscn":
			contadorWinsBlock+=1
	if contadorSpawn <= 0:
		$UI/SaveError/VBoxContainer/TypeError.text = "There is no Spawn Block"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	#if contadorWinsBlock <= 0:
		#$UI/SaveError/VBoxContainer/TypeError.text = "There is no Win Block"
		#$UI/SaveError.show()
		#$UI/Settings.hide()
		#return
	
	#Map
	var map = []
	if tilemap_level.get_children().size() > 0:
		for block in tilemap_level.get_children():
			map.append({
				"filePath" = block.scene_file_path,
				"position" = block.position
			})
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "There are no Blocks"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	
	#Cards
	Cards.clear()
	if $UI/VBoxContainer/MarginContainer/HBoxContainer/PanelContainer/GridContainer.get_children().size() > 0 or !Clown_Mode:
		for card in $UI/VBoxContainer/MarginContainer/HBoxContainer/PanelContainer/GridContainer.get_children():
			Cards.append({
				"filePath" = card.FilePath,
				"color" = card.color
			})
	else:
		$UI/SaveError/VBoxContainer/TypeError.text = "There are no Cards"
		$UI/SaveError.show()
		$UI/Settings.hide()
		return
	DeckButtons.clear()
	for cardButton in DeckcardsContainer.get_children():
		DeckButtons.append({
			"FilePath" = cardButton.FilePath,
			"Color" = cardButton.color,
			"Texture" = cardButton.texture_normal.resource_path
		})
	var LevelToSave = []
	
	LevelToSave.append({
		"Name" = Name,
		"Map" = map,
		"Cards" = Cards,
		"CardsButtons" = DeckButtons,
		"Rounds" = Rounds,
		"Lifes" = Lifes,
		"Public" = public 
	})
	
	if _file_exists_in_user_data("user://" + Name + ".clown"):
		$UI/OverWrite.Active("user://" + Name + ".clown",LevelToSave,true)
		return
	
	if public:
		var data = {
		"fields": {
			"Name": {"stringValue": Name},
			"Rounds": {"integerValue": Rounds},
			"Lifes": {"integerValue": Lifes},
			"Map": convertir_array_a_firestore_MAP(map),
			"Cards": convertir_array_a_firestore_CARDS(Cards),
			#"CardsButtons": {"stringValue": DeckButtons},
		}
		}
		print(data)
		fire_base_connector.SendData(data, Name)
	
	##SAVE
	var file = FileAccess.open("user://" + Name + ".clown" , FileAccess.WRITE)
	file.store_string(JSON.stringify(LevelToSave))
	file = null
	
	pathEditLevel = "user://" + Name + ".clown"
	await fire_base_connector.http_request.request_completed
	GAME_MANAGER.LoadLevelEditorMap(("user://" + Name + ".clown"))

#endregion
#endregion
func convertir_array_a_firestore_MAP(array: Array) -> Dictionary:
	var array_result = {"arrayValue": {"values": []}}
	for item in array:
		var firestore_item = {
		"mapValue": {
		"fields": {
		"filePath": {"stringValue": item["filePath"]},
		"position": {
		"mapValue": {
		"fields": {
		"x": {"integerValue": int(item["position"].x)},
		"y": {"integerValue": int(item["position"].y)}
		}
		}
		}
		}
		}
		}
		array_result["arrayValue"]["values"].append(firestore_item)
	return array_result
func convertir_array_a_firestore_CARDS(array: Array) -> Dictionary:
	var array_result = {"arrayValue": {"values": []}}
	for item in array:
		var firestore_item = {
		"mapValue": {
		"fields": {
		"filePath": {"stringValue": item["filePath"]},
		"color": {"stringValue": item["color"]}
		}
		}
		}
		array_result["arrayValue"]["values"].append(firestore_item)
	return array_result
