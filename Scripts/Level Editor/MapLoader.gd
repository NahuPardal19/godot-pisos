extends Node2D

var file 
var LevelToLoad 
#Rounds
var Rounds : int 
#Lifes
var Lifes : int 
#Cards
var Deck
#Map
var Map 
@onready var fire_base_connector: FireBase_Manager = $"FireBase Connector"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	GAME_MANAGER.OnLevelEditorMode = true
	if GAME_MANAGER.IsOnline:
		Online_LoadObjects(GAME_MANAGER.MapName)
	else:
		LoadObjects(GAME_MANAGER._File_To_Load)
	#print("------------------")
	#print(file)
	#print(LevelToLoad)
	#print(Rounds)
	#print(Lifes)
	#print(Map)
	#print("------------------")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func LoadObjects(path:String):
	if !GAME_MANAGER.Level_Loaded:
		file = FileAccess.open(path,FileAccess.READ)
		LevelToLoad = JSON.parse_string(file.get_as_text())
		
		$UI/Transition.LevelName = LevelToLoad[0]["Name"]
		print($UI/Transition.LevelName)
		#Rounds
		Rounds = LevelToLoad[0]["Rounds"]
		#Lifes
		Lifes = LevelToLoad[0]["Lifes"]
		#Cards
		Deck = LevelToLoad[0]["Cards"]
		#Map
		Map = LevelToLoad[0]["Map"]
		
		GAME_MANAGER.InitializeLevelEditorMap(Rounds,Lifes,Map,Deck,$Mapa/TilemapLevel,$UI/Deck)
		


func Online_LoadObjects(MapName:String):
	$UI/Transition.LevelName = GAME_MANAGER.Online_Level["fields"]["Name"]["stringValue"]
	
	Rounds = int(GAME_MANAGER.Online_Level["fields"]["Rounds"]["integerValue"])
	#Lifes
	Lifes = int(GAME_MANAGER.Online_Level["fields"]["Lifes"]["integerValue"])
	#Cards
	if  GAME_MANAGER.Online_Level["fields"]["Cards"]["arrayValue"].size() > 0:
		Deck = GAME_MANAGER.Online_Level["fields"]["Cards"]["arrayValue"]["values"]
	else:
		Deck = []
	#Map
	Map = GAME_MANAGER.Online_Level["fields"]["Map"]["arrayValue"]["values"]

	GAME_MANAGER.InitializeLevelEditorMap(Rounds,Lifes,Map,Deck,$Mapa/TilemapLevel,$UI/Deck)
	
