extends TextureButton

@onready var outline: TextureRect = $Outline
@export_dir var stringPath
@export var IsConnector : bool
@export var IsPreparation : bool
@export var CanConnect : bool
@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer

func _ready() -> void:
	pass # Replace with function body.

func _process(delta: float) -> void:
	if IsPreparation and LevelEditor.DeckcardsContainer.get_children().size() <= 0:
		disabled = true
		texture_focused = texture_disabled
	else:
		if IsPreparation:
			texture_focused = texture_normal
			disabled = false


#func _on_mouse_entered() -> void:
	#outline.show()
	#grab_focus()

#func _on_mouse_exited() -> void:
	#outline.hide()
	#release_focus()

func _on_pressed() -> void:
	audio_stream_player.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player.play()
	outline.show()
	grab_focus()
	LevelEditor.FloorPath = stringPath
	LevelEditor.ActualFloorIsConnector = IsConnector
	LevelEditor.ActualFloorIsPreparation = IsPreparation
	LevelEditor.ActualFloorCanConnect = CanConnect

#func _on_focus_entered() -> void:
	#audio_stream_player.stream = AUDIO_MANAGER.SelectedButton
	#audio_stream_player.play()

func _on_focus_exited() -> void:
	outline.hide()
	release_focus()
