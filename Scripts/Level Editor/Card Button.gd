extends TextureButton

class_name Card_Button

var CardToPut = preload("res://Scenes/Level Editor/Deck Card Button.tscn")

enum ColorCard_Enum{White,Green,Yellow}
var ColorCard : ColorCard_Enum

@export_dir var CardPath

@export_group("BUTTON CARD")

@export var White_Texture_Button : Texture
@export var Yellow_Texture_Button : Texture
@export var Green_Texture_Button : Texture

@export_group("DECK CARD")

@export var White_Texture : Texture
@export var Yellow_Texture : Texture
@export var Green_Texture : Texture

func _ready() -> void:
	pass # Replace with function body.

func _process(delta: float) -> void:
	pass
func _on_pressed() -> void:
	if LevelEditor.DeckcardsContainer.columns > LevelEditor.DeckcardsContainer.get_children().size():
		var CardColorString : String
		var cardInstance : CardButton_LevelEditor = CardToPut.instantiate()
		if ColorCard == ColorCard_Enum.White:
			cardInstance.texture_normal = White_Texture
			cardInstance.texture_disabled = White_Texture
			cardInstance.texture_focused = White_Texture
			cardInstance.texture_hover = White_Texture
			cardInstance.texture_pressed = White_Texture
			CardColorString = "white"
		elif ColorCard == ColorCard_Enum.Green:
			cardInstance.texture_normal = Green_Texture
			cardInstance.texture_disabled = Green_Texture
			cardInstance.texture_focused = Green_Texture
			cardInstance.texture_hover = Green_Texture
			cardInstance.texture_pressed = Green_Texture
			CardColorString = "green"
		elif ColorCard == ColorCard_Enum.Yellow:
			cardInstance.texture_normal = Yellow_Texture
			cardInstance.texture_disabled = Yellow_Texture
			cardInstance.texture_focused = Yellow_Texture
			cardInstance.texture_hover = Yellow_Texture
			cardInstance.texture_pressed = Yellow_Texture
			CardColorString = "yellow"
		cardInstance.color = CardColorString
		cardInstance.FilePath = CardPath
		LevelEditor.DeckcardsContainer.add_child(cardInstance)
	
func ChangeToColor(color : ColorCard_Enum):
	ColorCard = color
	if color == ColorCard_Enum.White:
		texture_normal = White_Texture_Button
		texture_disabled = White_Texture_Button
		texture_focused = White_Texture_Button
		texture_hover = White_Texture_Button
		texture_pressed = White_Texture_Button
	elif color == ColorCard_Enum.Green:
		texture_normal = Green_Texture_Button
		texture_disabled = Green_Texture_Button
		texture_focused = Green_Texture_Button
		texture_hover = Green_Texture_Button
		texture_pressed = Green_Texture_Button
	elif color == ColorCard_Enum.Yellow:
		texture_normal = Yellow_Texture_Button
		texture_disabled = Yellow_Texture_Button
		texture_focused = Yellow_Texture_Button
		texture_hover = Yellow_Texture_Button
		texture_pressed = Yellow_Texture_Button
