extends Node2D

@export var grid_color: Color = Color(1, 1, 1, 0.5)
@onready var tilemap_level: TileMap = $"../TilemapLevel"
var cell_size: Vector2
@onready var camera: Camera2D = $"../Camera2D"

func _ready():
	cell_size = tilemap_level.tile_set.tile_size

func _draw():
	var viewport = get_viewport()
	var viewport_rect = viewport.get_visible_rect()
	var width = viewport_rect.size.x
	var height = viewport_rect.size.y

	var camera_pos 
	if camera:
		camera_pos = camera.get_position()
	else:
		camera_pos = Vector2.ZERO

	# Calcula el desplazamiento de la cuadrícula en base a la posición de la cámara

	var offset_x = int(camera_pos.x) % int(cell_size.x)
	var offset_y = int(camera_pos.y) % int(cell_size.y)

	# Dibuja las líneas verticales de la cuadrícula
	for x in range(cell_size.x/2, width + cell_size.x, cell_size.x):
		draw_line(Vector2(x , 0), Vector2(x - offset_x, height), grid_color)
	

	for y in range(cell_size.y/2, height + cell_size.y, cell_size.y):
		draw_line(Vector2(0, y - offset_y), Vector2(width, y - offset_y), grid_color)


func _process(delta):
	queue_redraw()
