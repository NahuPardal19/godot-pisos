extends TextureButton

class_name CardButton_LevelEditor

@onready var outline: TextureRect = $Outline
var selected : bool = false
var FilePath
var color : String

func _ready() -> void:
	pass # Replace with function body.

func _process(delta: float) -> void:
	pass

func Select():
	for card in get_parent().get_children():
		if card.selected and get_index() != card.get_index():
			_swap_cards(card.get_index(),get_index())
			for cardP in get_parent().get_children():
				cardP.outline.hide()
				cardP.selected = false
			return
	if selected:
		selected = false
		outline.hide()
	else:
		selected = true
		outline.show()

func _gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == MOUSE_BUTTON_LEFT:
				Select()
			elif event.button_index == MOUSE_BUTTON_RIGHT:
				queue_free()

func _swap_cards(index1,index2):
		var parent = get_parent()
		var child1 = parent.get_child(index1)
		var child2 = parent.get_child(index2)
			
		if child1 and child2:
			if index1 < index2:
				parent.add_child(child1)
				parent.move_child(child1, index2)
				parent.add_child(child2)
				parent.move_child(child2, index1)
			else:
				parent.add_child(child2)
				parent.move_child(child2, index1)
				parent.add_child(child1)
				parent.move_child(child1, index2)
