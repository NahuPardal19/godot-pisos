extends TextureButton
@onready var audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer
var levelName
@onready var label: Label = $Label

var NormalTexture = preload("res://Sprites/UI SPRITES/Button sprites/Button normal.png")
var PressedTexture = preload("res://Sprites/UI SPRITES/Button sprites/Button Pressed.png")
var OnMouseTexture = preload("res://Sprites/UI SPRITES/Button sprites/Button Disabled.png")

func _ready() -> void:
	name = levelName
	label.text = levelName

func _process(delta: float) -> void:
	pass

func _on_pressed() -> void:
	for levelbutton in get_parent().get_children():
		levelbutton.texture_normal = NormalTexture
		levelbutton.texture_pressed = PressedTexture
		levelbutton.texture_hover = OnMouseTexture
		levelbutton.texture_focused = PressedTexture
	
	texture_normal = PressedTexture
	texture_pressed = NormalTexture
	texture_hover = PressedTexture
	texture_focused = PressedTexture
	
	audio_stream_player.stream = AUDIO_MANAGER.PressedButton
	audio_stream_player.play()
	
	if GAME_MANAGER.IsOnline:
		GAME_MANAGER.MapName = levelName
	else:
		GAME_MANAGER.Menu_Map_File = "user://"+levelName+".clown"

#func _on_mouse_entered() -> void:
	#grab_focus()

func _on_focus_entered() -> void:
	audio_stream_player.stream = AUDIO_MANAGER.SelectedButton
	audio_stream_player.play()
